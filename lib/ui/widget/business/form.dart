import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/util/image_upload.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:wameiji/ui/widget/image_wrap.dart';

class FormTitleValue extends StatelessWidget {
  final String? title;
  final String? value;

  const FormTitleValue({this.title, this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      constraints: const BoxConstraints(minHeight: 50),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 6),
            child: Text(
              title ?? '',
              style: MyText.textStyle13_000,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Expanded(
            child: Text(
              value ?? '',
              style: MyText.textStyle13_000,
              textAlign: TextAlign.end,
            ),
          )
        ],
      ),
    );
  }
}

class FormTitleEdit extends StatelessWidget {
  final String? title;
  final TextEditingController? textController;
  final TextInputType? keyboardType;
  final String? hint;
  final int? maxLength;

  const FormTitleEdit({
    this.title,
    this.textController,
    this.keyboardType,
    this.hint = '请输入',
    this.maxLength,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 6),
            child: Text(
              title ?? '',
              style: MyText.textStyle13_000,
            ),
          ),
          Expanded(
            child: TextField(
              controller: textController,
              style: const TextStyle(
                fontSize: 13,
                color: Colors.black,
              ),
              keyboardType: keyboardType,
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                hintText: hint ?? '',
                hintStyle: const TextStyle(color: Color(0xffa6a6a6)),
                contentPadding: const EdgeInsets.symmetric(vertical: 8),
                border: const OutlineInputBorder(borderSide: BorderSide.none),
              ),
              inputFormatters: [
                if (maxLength != null)
                  LengthLimitingTextInputFormatter(maxLength)
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class FormTitleSel extends StatelessWidget {
  final String? title;
  final String? value;
  final String? hint;
  final GestureTapCallback? onTap;

  const FormTitleSel({
    this.title,
    this.hint = '请选择',
    this.value,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      constraints: const BoxConstraints(minHeight: 50),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 6),
            child: Text(
              title ?? '',
              style: MyText.textStyle13_000,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: onTap,
              behavior: HitTestBehavior.translucent,
              child: Text(
                value ?? (hint ?? ''),
                style: TextStyle(
                  fontSize: 13,
                  color: UiUtils.isEmpty(value)
                      ? const Color(0xffa6a6a6)
                      : Colors.black,
                ),
                textAlign: TextAlign.end,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 5),
            child: Image(image: AssetImage(Res.item_sel)),
          )
        ],
      ),
    );
  }
}

class FormTitleImage extends StatelessWidget {
  final String? title;
  final List<String>? urls;

  const FormTitleImage({
    this.title,
    this.urls,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 30),
            child: Text(
              title ?? '',
              style: MyText.textStyle13_000,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Expanded(
            child: ImageWrap(
              urls,
              isRight: true,
            ),
          ),
        ],
      ),
    );
  }
}

class FormTitleSelImage extends StatefulWidget {
  final String? title;
  final String? notice;
  final List<String> urls;
  final int max;

  const FormTitleSelImage({
    this.title,
    this.notice,
    required this.urls,
    this.max = 1,
  });

  @override
  State<StatefulWidget> createState() {
    return _FormTitleSelImageState();
  }
}

class _FormTitleSelImageState extends State<FormTitleSelImage> {
  onAddImage() async {
    var assets = await LibUtils.selPic(context, widget.max - widget.urls.length);
    if (assets == null || assets.isEmpty) return;
    // 压缩上传
    LibUtils.showLoading();
    for (var i = 0; i < assets.length; ++i) {
      var o = assets[i];
      try {
        var value = await ImageUploader(o, true).start();
        if (value == null) break;
        setState(() {
          widget.urls.add(value);
        });
      } catch (error) {
        if (error is HttpException) {
          LibUtils.showToast(error.msg);
        }
        break;
      }
    }
    LibUtils.hideLoading();
  }

  onRemoveImage(int index) {
    setState(() {
      widget.urls.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(5, 10, 12, 0),
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 6, left: 7),
            child: Row(
              children: [
                Text(
                  widget.title ?? '',
                  style: MyText.textStyle13_000,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(
                      widget.notice ?? '',
                      textAlign: TextAlign.end,
                      style: MyText.textStyle12_999,
                    ),
                  ),
                ),
              ],
            ),
          ),
          GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4, childAspectRatio: 1),
            itemBuilder: (context, index) {
              if (index >= widget.urls.length) {
                return Container(
                  padding: const EdgeInsets.all(10),
                  child: GestureDetector(
                    child: const Image(
                      image: AssetImage(Res.ic_ninegrid_addmore),
                      fit: BoxFit.cover,
                    ),
                    onTap: () {
                      onAddImage();
                    },
                    behavior: HitTestBehavior.translucent,
                  ),
                );
              } else {
                return Stack(
                  children: <Widget>[
                    Positioned(
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10,
                        child: GestureDetector(
                          child: UiUtils.getNetImageFit(widget.urls[index],
                              fit: BoxFit.cover, cacheWidthRate: .3),
                          onTap: () {
                            List galleryItems = widget.urls.map((e) {
                              Uri u = Uri.parse(e);
                              if (u.scheme.length == 0) {
                                return HttpClient.baseUrl + e;
                              }
                              return e;
                            }).toList();

                            UiUtils.showImage(context, galleryItems, index);
                          },
                          behavior: HitTestBehavior.translucent,
                        )),
                    Positioned(
                        right: 0,
                        top: 0,
                        child: GestureDetector(
                          child: const Image(
                              image: AssetImage(Res.ic_ninegridimage_delete)),
                          onTap: () {
                            onRemoveImage(index);
                          },
                          behavior: HitTestBehavior.translucent,
                        )),
                  ],
                );
              }
            },
            itemCount: widget.urls.length >= widget.max
                ? widget.max
                : widget.urls.length + 1,
          ),
        ],
      ),
    );
  }
}

class FormTitleValueTap extends StatelessWidget {
  final String? title;
  final String? value;
  final GestureTapCallback? onTap;

  const FormTitleValueTap({this.title, this.value, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(left: 10),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 6),
            child: Text(
              title ?? '',
              style: MyText.textStyle13_000,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const Spacer(),
          ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 150,
            ),
            child: GestureDetector(
              onTap: onTap,
              behavior: HitTestBehavior.translucent,
              child: Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                child: Text(
                  value ?? '',
                  style:
                  const TextStyle(color: Color(0xff5878b4), fontSize: 13.0),
                  textAlign: TextAlign.end,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
