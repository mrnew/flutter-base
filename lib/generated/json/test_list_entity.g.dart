import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/test_list_entity.dart';

TestListEntity $TestListEntityFromJson(Map<String, dynamic> json) {
	final TestListEntity testListEntity = TestListEntity();
	final List<TestListListEntity?>? list = (json['list'] as List?)?.map((e) => TestListListEntity.fromJson(e)).cast<TestListListEntity?>().toList();
	if (list != null) {
		testListEntity.list = list;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		testListEntity.total = total;
	}
	return testListEntity;
}

Map<String, dynamic> $TestListEntityToJson(TestListEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['list'] =  entity.list?.map((v) => v?.toJson()).toList();
	data['total'] = entity.total;
	return data;
}

TestListListEntity $TestListListEntityFromJson(Map<String, dynamic> json) {
	final TestListListEntity testListListEntity = TestListListEntity();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		testListListEntity.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		testListListEntity.name = name;
	}
	return testListListEntity;
}

Map<String, dynamic> $TestListListEntityToJson(TestListListEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}