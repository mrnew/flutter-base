import 'dart:convert';

import 'package:get/get.dart';
import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/mercari_home_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';

import 'state.dart';

class IndexLogic extends BaseLogic<MercariHomeEntity> with StateMixin {
  final IndexState data = IndexState();

  @override
  void onReady() async {
    super.onReady();
    if (!isReadyLoadData) {
      Future.delayed(Duration.zero, onAutoRefresh);
    }
  }

  @override
  void onInit() async {
    var str = await CacheManager.getInstance().get(false, 'MainData', null);
    if (str != null) {
      MercariHomeEntity mer = JsonConvert.fromJsonAsT(
          jsonDecode(str), (MercariHomeEntity).toString());
      data.entity = mer;
      isReadyLoadData = false;
      change(mer, status: RxStatus.success());
    }
    super.onInit();
  }

  @override
  void getData() async {
    var param = {
      'n': 'Sig.Front.SubSite.AppMercari.MercariHome',
    };
    HttpClient.instance.request<MercariHomeEntity>(param,
        cancelToken: cancelToken, success: (MercariHomeEntity? entity) {
      data.entity = entity;
      CacheManager.getInstance().put(false, 'MainData', jsonEncode(entity));
      onPageSuccess(entity);
    }, error: (HttpException e) {
      onPageFaild(e);
    });
  }
}
