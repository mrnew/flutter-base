import 'dart:async';
import 'dart:convert';

import 'package:get/get.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/main.dart';
import 'package:wameiji/model/open_ad_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/main/state.dart';

import 'state.dart';

class SpalshLogic extends BaseLogic {
  final SpalshState data = SpalshState();

  @override
  void onReady() {
    super.onReady();
    CacheManager.getInstance().get(false, 'OpenAdEntity', null).then((value) {
      Future.delayed(const Duration(seconds: 1), () {
        if (value != null) {
          data.entity = JsonConvert.fromJsonAsT(
              jsonDecode(value), (OpenAdEntity).toString());
          if (data.entity != null) {
            data.count.value = (data.entity?.waitSecond ?? 3);
            startTime();
          }
        }
        data.isReady.value = true;
      });
    });
  }

  @override
  void onClose() {
    super.onClose();
    _timer?.cancel();
  }

  Timer? _timer;

  void startTime() async {
    //设置启动图生效时间
    var _duration = const Duration(milliseconds: 500);
    Timer(_duration, () {
      // 空等1秒之后再计时
      _timer = Timer.periodic(const Duration(milliseconds: 1000), (v) {
        data.count.value--;
        if (data.count.value == 0) {
          initUmeng();
          if (GlobalConfig.isMustLogin && !UserManager.isLogin()) {
            Get.offNamed('login_page');
            return;
          }
          Get.offNamed('main_page');
        }
      });
    });
  }

  void goMain() {
    _timer?.cancel();
    initUmeng();
    _doGoPage();
  }

  void goH5() {
    _timer?.cancel();
    initUmeng();
    MainState.entity = data.entity;
    _doGoPage();
  }

  _doGoPage() {
    if (GlobalConfig.isInitUser) {
      if (GlobalConfig.isMustLogin && !UserManager.isLogin()) {
        Get.offNamed('login_page');
        return;
      }
      Get.offNamed('main_page');
    } else {
      _timer = Timer.periodic(const Duration(milliseconds: 100), (v) {
        if (GlobalConfig.isInitUser) {
          if (GlobalConfig.isMustLogin && !UserManager.isLogin()) {
            Get.offNamed('login_page');
            return;
          }
          Get.offNamed('main_page');
        }
      });
    }
  }
}
