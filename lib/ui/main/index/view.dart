import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:mrnew_libary/widget/keep_alive.dart';
import 'package:wameiji/model/mercari_home_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/util/page_utils.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'logic.dart';
import 'state.dart';

class IndexPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<IndexPage, IndexLogic, IndexState> {
  IndexLogic createLogic() {
    return IndexLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(context, logic, () {
      return _getContent(logic, state);
    },
        title: '首页',
        isRefersh: true,
        state: state,
        bg: Colors.white,
        enablePullUp: false,
        elevation: 0,
        hasAppBarLeading: false);
  }
}

Widget _getContent(IndexLogic logic, IndexState state) {
  var slivers = <Widget>[];
  slivers.add(_getBanner(logic, state, state.entity!.slider!));
  slivers.add(_getExp(logic, state));
  return CustomScrollView(
    slivers: slivers,
    cacheExtent: 300,
  );
}

Widget _getBanner(
    IndexLogic logic, IndexState state, List<MercariHomeSlider> list) {
  var banner = KeepAliveWidget(
    AspectRatio(
      aspectRatio: 375 / 160,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          var data = list[index];
          return GestureDetector(
              onTap: () async {
                if (data.url?.startsWith("alipays://") ?? false) {
                  PageUtils.startApp(data.url!, 0);
                  return;
                }
                if (data.url?.startsWith("taobao://") ?? false) {
                  PageUtils.startApp(data.url!, 1);
                  return;
                }
                PageUtils.startWeb(data.url, null);
              },
              behavior: HitTestBehavior.translucent,
              child: UiUtils.getNetImageFit(data.slideImage,
                  fadeInDuration: Duration.zero,
                  fadeOutDuration: Duration.zero,
                  cacheWidthRate: 1,
                  useWhitePlaceholder: true));
        },
        itemCount: list.length,
        autoplay: list.length > 1,
        autoplayDelay: 5000,
        loop: true,
        pagination: const SwiperPagination(
            builder: DotSwiperPaginationBuilder(
                size: 5,
                activeSize: 5,
                space: 5,
                activeColor: Color(0xfff86172),
                color: Colors.white)),
        viewportFraction: 1,
        indicatorLayout: PageIndicatorLayout.SCALE,
      ),
    ),
  );
  return SliverToBoxAdapter(
    child: banner,
  );
}

_getExp(IndexLogic logic, IndexState state) {
  return SliverFixedExtentList(
      itemExtent: 180,
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return _getItem();
        },
        childCount: 20,
      ));
}

Widget _getItem() {
  return GestureDetector(
      onTap: () {},
      behavior: HitTestBehavior.translucent,
      child: Container(
        padding: const EdgeInsets.fromLTRB(10, 15, 10, 0),
        decoration: const BoxDecoration(
            border: Border(bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const Text(
                '12',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 15,
                    color: Color(0xff202228),
                    fontWeight: FontWeight.w500),
                maxLines: 1,
              ),
              Expanded(
                  child: UiUtils.getNetImageFit(
                      'https://up.enterdesk.com/edpic_source/23/ea/d0/23ead0213611853b943df8a5e2c98454.jpg',
                      fit: BoxFit.cover))
            ]),
      ));
}
