import 'package:flutter/material.dart';
import 'package:wameiji/ui/base/page.dart';

import 'logic.dart';
import 'state.dart';

class MinePage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<MinePage, MineLogic, MineState> {
  @override
  MineLogic createLogic() {
    return MineLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(context, logic, () {
      var children = <Widget>[];
      return Container(
        color: Colors.white,
        child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: children,
            )),
      );
    });
  }
}
