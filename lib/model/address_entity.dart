import 'package:wameiji/generated/json/base/json_field.dart';
import 'package:wameiji/generated/json/address_entity.g.dart';


@JsonSerializable()
class AddressEntity {

	AddressEntity();

	factory AddressEntity.fromJson(Map<String, dynamic> json) => $AddressEntityFromJson(json);

	Map<String, dynamic> toJson() => $AddressEntityToJson(this);

  List<AddressAddres?>? address;
  String? current;
}

@JsonSerializable()
class AddressAddres {

	AddressAddres();

	factory AddressAddres.fromJson(Map<String, dynamic> json) => $AddressAddresFromJson(json);

	Map<String, dynamic> toJson() => $AddressAddresToJson(this);

  String? id;
  String? realName;
  String? phoneNumber;
  String? country;
  String? province;
  String? city;
  String? district;
  String? address;
  String? zipCode;
  bool? isCurrent;
}
