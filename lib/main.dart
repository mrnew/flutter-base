import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:leak_detector/leak_detector.dart';
import 'package:wameiji/app.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/main/logic.dart';
import 'package:wameiji/ui/main/state.dart';
import 'package:wameiji/ui/util/page_utils.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:worker_manager/worker_manager.dart';

import 'config.dart';

main() async {
  //初始化线程池
  await workerManager.init();
  runApp(createApp());
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    systemNavigationBarColor: Color(0xFF000000),
    systemNavigationBarDividerColor: null,
    statusBarColor: Colors.transparent,
    systemNavigationBarIconBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness: Brightness.light,
  ));
  GlobalConfig.init();
  _initUser().then((value) async {

  });
  _initCheckMemory();
}

void _initCheckMemory() {
  if (GlobalConfig.isTest && GlobalConfig.isCheckMemory) {
    LeakDetector()
      ..init(maxRetainingPath: 300)
      ..onLeakedStream.listen((LeakedInfo info) {
        if (!GlobalConfig.isCheckMemory) return;
        if (info.retainingPath.length > 1) {
          var src = info.retainingPath[1];
          if (src.clazz == "GetPageRoute" &&
              src.sourceCodeLocation?.lineNum == 92) {
            return;
          }
        }
        info.retainingPath.forEach((node) => print(node));
        //show preview page
        showLeakedInfoPage(Get.context!, info);
      });
  }
}

void initUmeng() async {
}

Future<void> _initUser() async {
  //初始化网络
  await CacheManager.getInstance().openDb();
  await HttpClient.instance.init();
  await UserManager.init();
  GlobalConfig.isInitUser = true;
}
