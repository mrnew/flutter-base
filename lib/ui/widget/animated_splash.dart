library animated_splash;

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/res.dart';

Function? _customFunction;
late int _duration;
AnimatedSplashType? _runfor;

enum AnimatedSplashType { StaticDuration, BackgroundProcess }

class AnimatedSplash extends StatefulWidget {
  AnimatedSplash(
      {Function? customFunction,
      required int duration,
      AnimatedSplashType? type}) {
    _duration = duration;
    _customFunction = customFunction;
    _runfor = type;
  }

  @override
  _AnimatedSplashState createState() => _AnimatedSplashState();
}

const List<String> images = [
  Res.splash_bg,
];

class _AnimatedSplashState extends State<AnimatedSplash>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _animation;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    // if (_duration < 1000) _duration = 2000;
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: _duration));
    _animation = Tween(begin: 1.0, end: 1.0).animate(
        CurvedAnimation(parent: _animationController, curve: Curves.linear));
    _animationController.forward();

    _timer = _runfor == AnimatedSplashType.BackgroundProcess
        ? Timer(Duration.zero, () {
            _timer = Timer(Duration(milliseconds: _duration), () {
              _customFunction!(context);
            });
          })
        : Timer(Duration(milliseconds: _duration), () {
            _customFunction!(context);
          });
  }

  @override
  void dispose() {
    _timer?.cancel();
    _animationController.reset();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    images.forEach((element) {
      precacheImage(AssetImage(element), context);
    });
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(alignment: Alignment.topCenter, children: [
          if (GlobalConfig.hasSplash)
            Positioned.fill(
                child: FadeTransition(
                    opacity: _animation as Animation<double>,
                    child: Image.asset(
                      Res.splash_bg,
                      fit: BoxFit.scaleDown,
                    ))),
          if (GlobalConfig.hasSplash)
            const Positioned(
                bottom: 0,
                height: 130,
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: Text(
                      'Copyright © 2020-2022 xxx 版权所有',
                      style: TextStyle(
                        fontSize: 11,
                        color: Color(0xff9B9B9B),
                      ),
                    ),
                  ),
                ))
        ]));
  }
}
