import 'package:wameiji/generated/json/base/json_field.dart';
import 'package:wameiji/generated/json/test_list_entity.g.dart';


@JsonSerializable()
class TestListEntity {

	TestListEntity();

	factory TestListEntity.fromJson(Map<String, dynamic> json) => $TestListEntityFromJson(json);

	Map<String, dynamic> toJson() => $TestListEntityToJson(this);

  List<TestListListEntity?>? list;
  int? total;
}

@JsonSerializable()
class TestListListEntity {

	TestListListEntity();

	factory TestListListEntity.fromJson(Map<String, dynamic> json) => $TestListListEntityFromJson(json);

	Map<String, dynamic> toJson() => $TestListListEntityToJson(this);

  String? id;
  String? name;
}
