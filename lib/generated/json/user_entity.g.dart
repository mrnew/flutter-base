import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/user_entity.dart';

UserEntity $UserEntityFromJson(Map<String, dynamic> json) {
	final UserEntity userEntity = UserEntity();
	final String? localId = jsonConvert.convert<String>(json['localId']);
	if (localId != null) {
		userEntity.localId = localId;
	}
	final String? avatar = jsonConvert.convert<String>(json['avatar']);
	if (avatar != null) {
		userEntity.avatar = avatar;
	}
	final String? birthday = jsonConvert.convert<String>(json['birthday']);
	if (birthday != null) {
		userEntity.birthday = birthday;
	}
	final String? email = jsonConvert.convert<String>(json['email']);
	if (email != null) {
		userEntity.email = email;
	}
	final String? mobile = jsonConvert.convert<String>(json['mobile']);
	if (mobile != null) {
		userEntity.mobile = mobile;
	}
	final String? nickname = jsonConvert.convert<String>(json['nickname']);
	if (nickname != null) {
		userEntity.nickname = nickname;
	}
	final String? sex = jsonConvert.convert<String>(json['sex']);
	if (sex != null) {
		userEntity.sex = sex;
	}
	final String? token = jsonConvert.convert<String>(json['token']);
	if (token != null) {
		userEntity.token = token;
	}
	final bool? hasCompleteInfo = jsonConvert.convert<bool>(json['hasCompleteInfo']);
	if (hasCompleteInfo != null) {
		userEntity.hasCompleteInfo = hasCompleteInfo;
	}
	return userEntity;
}

Map<String, dynamic> $UserEntityToJson(UserEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['localId'] = entity.localId;
	data['avatar'] = entity.avatar;
	data['birthday'] = entity.birthday;
	data['email'] = entity.email;
	data['mobile'] = entity.mobile;
	data['nickname'] = entity.nickname;
	data['sex'] = entity.sex;
	data['token'] = entity.token;
	data['hasCompleteInfo'] = entity.hasCompleteInfo;
	return data;
}