import 'dart:convert';

import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/open_ad_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/util/page_utils.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'state.dart';

class MainLogic extends BaseLogic {
  final MainState data = MainState();

  @override
  void onInit() async {
    super.onInit();
    //只有第一次申请
    var tempStr =
        await CacheManager.getInstance().get(false, 'firstPermission', null);
    if (tempStr == null) {
      [
        Permission.storage,
      ].request();
      CacheManager.getInstance().put(false, 'firstPermission', 'true');
    }
    if (GetPlatform.isAndroid) {
      //检查更新
      Future.delayed(const Duration(seconds: 2), () {
        UiUtils.checkUpgradle(Get.context!, false);
      });
      if (MainState.pushType != null) {
        PageUtils.startPushPage(
          MainState.pushType,
          MainState.pushParam,
        );
        MainState.pushParam = null;
        MainState.pushType = null;
      }
    } else {}
    if (MainState.entity != null) {
      //点击了广告
      PageUtils.startWeb(MainState.entity?.url, null);
      MainState.entity = null;
    }
  }

  @override
  void onReady() {
    super.onReady();
    if (GlobalConfig.hasAdSplash) {
      getOpenAd();
    }
  }

  void getOpenAd() async {
    await CacheManager.getInstance().remove(false, 'OpenAdEntity');
    HttpClient.instance.request<OpenAdEntity>({
      'n': 'Sig.Front.AppFront.GetOpenAd',
    }, cancelToken: cancelToken, isShowErrorMsg: false,
        success: (OpenAdEntity? data) {
      if (UiUtils.isEmpty(data?.img)) return;
      CacheManager.getInstance().put(false, 'OpenAdEntity', jsonEncode(data));
      DefaultCacheManager().downloadFile(data!.img!);
    });
  }
}
