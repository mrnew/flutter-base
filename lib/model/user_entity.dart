import 'package:wameiji/generated/json/base/json_field.dart';
import 'package:wameiji/generated/json/user_entity.g.dart';


@JsonSerializable()
class UserEntity {

	UserEntity();

	factory UserEntity.fromJson(Map<String, dynamic> json) => $UserEntityFromJson(json);

	Map<String, dynamic> toJson() => $UserEntityToJson(this);

  String? localId;
  String? avatar;
  String? birthday;
  String? email;
  String? mobile;
  String? nickname;
  String? sex;
  String? token;
  bool? hasCompleteInfo;
}
