import 'dart:io' hide HttpClient;
import 'dart:math';

import 'package:common_utils/common_utils.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class ImageUploader {
  AssetEntity? asset;
  bool isWater;
  late String url;
  CroppedFile? file;
  int minWidth;
  int minHeight;
  int quality;

  ImageUploader(this.asset, this.isWater,
      {this.file,
        bool isValidate = false,
        this.minHeight = 600,
        this.minWidth = 600,
        this.quality = 80}) {
    if (isValidate) {
      url = "?from=" +
          (Platform.isAndroid ? 'android' : 'IOS') +
          "&n=Sig.Front.AppApi.UploadWithValidate";
    } else if (isWater) {
      url = "?from=" +
          (Platform.isAndroid ? 'android' : 'IOS') +
          "&n=Sig.Front.AppApi.UploadWithWaterMark";
    } else {
      url = "?from=" +
          (Platform.isAndroid ? 'android' : 'IOS') +
          "&n=Sig.Front.AppApi.Upload";
    }
  }

  bool isStart = false;

  Future<String?> start() async {
    if (asset == null && file == null) return null;
    isStart = true;
    var path;
    if (asset != null) {
      path = (await asset?.file)?.absolute.path;
    } else {
      path = file!.path;
    }
    //压缩图片
    Directory dir = Directory.systemTemp;
    String newpath = dir.path + "/myTemp.jpg";
    var newfile = File(newpath);
    bool exist = await newfile.exists();
    if (exist) newfile.deleteSync();
    var result = await FlutterImageCompress.compressAndGetFile(path, newpath,
        minWidth: minWidth, minHeight: minHeight, quality: quality);
    if (!isStart) return null;
    //上传图片
    var name = DateUtil.formatDate(DateTime.now(), format: 'yyyyMMddHHmmss');
    name += _randomBit(4) + ".jpg";
    if (result == null) return null;
    String? orginUrl = await HttpClient.instance.upload(url, result.path, name);
    if (!isStart) return null;
    return orginUrl;
  }

  end() {
    isStart = false;
  }

  _randomBit(int len) {
    String scopeF = '123456789'; //首位
    String scopeC = '0123456789'; //中间
    String result = '';
    for (int i = 0; i < len; i++) {
      if (i == 1) {
        result = scopeF[Random().nextInt(scopeF.length)];
      } else {
        result = result + scopeC[Random().nextInt(scopeC.length)];
      }
    }
    return result;
  }
}
