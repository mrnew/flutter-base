import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/address_entity.dart';

AddressEntity $AddressEntityFromJson(Map<String, dynamic> json) {
	final AddressEntity addressEntity = AddressEntity();
	final List<AddressAddres?>? address = (json['address'] as List?)?.map((e) => AddressAddres.fromJson(e)).cast<AddressAddres?>().toList();
	if (address != null) {
		addressEntity.address = address;
	}
	final String? current = jsonConvert.convert<String>(json['current']);
	if (current != null) {
		addressEntity.current = current;
	}
	return addressEntity;
}

Map<String, dynamic> $AddressEntityToJson(AddressEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['address'] =  entity.address?.map((v) => v?.toJson()).toList();
	data['current'] = entity.current;
	return data;
}

AddressAddres $AddressAddresFromJson(Map<String, dynamic> json) {
	final AddressAddres addressAddres = AddressAddres();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		addressAddres.id = id;
	}
	final String? realName = jsonConvert.convert<String>(json['realName']);
	if (realName != null) {
		addressAddres.realName = realName;
	}
	final String? phoneNumber = jsonConvert.convert<String>(json['phoneNumber']);
	if (phoneNumber != null) {
		addressAddres.phoneNumber = phoneNumber;
	}
	final String? country = jsonConvert.convert<String>(json['country']);
	if (country != null) {
		addressAddres.country = country;
	}
	final String? province = jsonConvert.convert<String>(json['province']);
	if (province != null) {
		addressAddres.province = province;
	}
	final String? city = jsonConvert.convert<String>(json['city']);
	if (city != null) {
		addressAddres.city = city;
	}
	final String? district = jsonConvert.convert<String>(json['district']);
	if (district != null) {
		addressAddres.district = district;
	}
	final String? address = jsonConvert.convert<String>(json['address']);
	if (address != null) {
		addressAddres.address = address;
	}
	final String? zipCode = jsonConvert.convert<String>(json['zipCode']);
	if (zipCode != null) {
		addressAddres.zipCode = zipCode;
	}
	final bool? isCurrent = jsonConvert.convert<bool>(json['isCurrent']);
	if (isCurrent != null) {
		addressAddres.isCurrent = isCurrent;
	}
	return addressAddres;
}

Map<String, dynamic> $AddressAddresToJson(AddressAddres entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['realName'] = entity.realName;
	data['phoneNumber'] = entity.phoneNumber;
	data['country'] = entity.country;
	data['province'] = entity.province;
	data['city'] = entity.city;
	data['district'] = entity.district;
	data['address'] = entity.address;
	data['zipCode'] = entity.zipCode;
	data['isCurrent'] = entity.isCurrent;
	return data;
}