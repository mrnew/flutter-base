import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/mercari_home_entity.dart';

MercariHomeEntity $MercariHomeEntityFromJson(Map<String, dynamic> json) {
	final MercariHomeEntity mercariHomeEntity = MercariHomeEntity();
	final List<MercariHomeActivity>? activity = (json['activity'] as List?)?.map((e) => MercariHomeActivity.fromJson(e)).cast<MercariHomeActivity>().toList();
	if (activity != null) {
		mercariHomeEntity.activity = activity;
	}
	final List<MercariHomeActivity>? activitySmall = (json['activitySmall'] as List?)?.map((e) => MercariHomeActivity.fromJson(e)).cast<MercariHomeActivity>().toList();
	if (activitySmall != null) {
		mercariHomeEntity.activitySmall = activitySmall;
	}
	final MercariHomeAnnouncement? announcement = jsonConvert.convert<MercariHomeAnnouncement>(json['announcement']);
	if (announcement != null) {
		mercariHomeEntity.announcement = announcement;
	}
	final MercariHomeLink? link = jsonConvert.convert<MercariHomeLink>(json['link']);
	if (link != null) {
		mercariHomeEntity.link = link;
	}
	final List<MercariHomeCat>? cat = (json['cat'] as List?)?.map((e) => MercariHomeCat.fromJson(e)).cast<MercariHomeCat>().toList();
	if (cat != null) {
		mercariHomeEntity.cat = cat;
	}
	final List<MercariHomeHotGoods>? hotGoods = (json['hotGoods'] as List?)?.map((e) => MercariHomeHotGoods.fromJson(e)).cast<MercariHomeHotGoods>().toList();
	if (hotGoods != null) {
		mercariHomeEntity.hotGoods = hotGoods;
	}
	final List<MercariHomeSlider>? slider = (json['slider'] as List?)?.map((e) => MercariHomeSlider.fromJson(e)).cast<MercariHomeSlider>().toList();
	if (slider != null) {
		mercariHomeEntity.slider = slider;
	}
	final List<MercariHomeArticle>? article = (json['article'] as List?)?.map((e) => MercariHomeArticle.fromJson(e)).cast<MercariHomeArticle>().toList();
	if (article != null) {
		mercariHomeEntity.article = article;
	}
	return mercariHomeEntity;
}

Map<String, dynamic> $MercariHomeEntityToJson(MercariHomeEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['activity'] =  entity.activity?.map((v) => v.toJson()).toList();
	data['activitySmall'] =  entity.activitySmall?.map((v) => v.toJson()).toList();
	data['announcement'] = entity.announcement?.toJson();
	data['link'] = entity.link?.toJson();
	data['cat'] =  entity.cat?.map((v) => v.toJson()).toList();
	data['hotGoods'] =  entity.hotGoods?.map((v) => v.toJson()).toList();
	data['slider'] =  entity.slider?.map((v) => v.toJson()).toList();
	data['article'] =  entity.article?.map((v) => v.toJson()).toList();
	return data;
}

MercariHomeAnnouncement $MercariHomeAnnouncementFromJson(Map<String, dynamic> json) {
	final MercariHomeAnnouncement mercariHomeAnnouncement = MercariHomeAnnouncement();
	final String? contentPlain = jsonConvert.convert<String>(json['ContentPlain']);
	if (contentPlain != null) {
		mercariHomeAnnouncement.contentPlain = contentPlain;
	}
	final String? id = jsonConvert.convert<String>(json['Id']);
	if (id != null) {
		mercariHomeAnnouncement.id = id;
	}
	final String? title = jsonConvert.convert<String>(json['Title']);
	if (title != null) {
		mercariHomeAnnouncement.title = title;
	}
	return mercariHomeAnnouncement;
}

Map<String, dynamic> $MercariHomeAnnouncementToJson(MercariHomeAnnouncement entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['ContentPlain'] = entity.contentPlain;
	data['Id'] = entity.id;
	data['Title'] = entity.title;
	return data;
}

MercariHomeLink $MercariHomeLinkFromJson(Map<String, dynamic> json) {
	final MercariHomeLink mercariHomeLink = MercariHomeLink();
	final String? feeDesc = jsonConvert.convert<String>(json['feeDesc']);
	if (feeDesc != null) {
		mercariHomeLink.feeDesc = feeDesc;
	}
	final String? noviceStrategy = jsonConvert.convert<String>(json['noviceStrategy']);
	if (noviceStrategy != null) {
		mercariHomeLink.noviceStrategy = noviceStrategy;
	}
	final String? shipmentFee = jsonConvert.convert<String>(json['shipmentFee']);
	if (shipmentFee != null) {
		mercariHomeLink.shipmentFee = shipmentFee;
	}
	return mercariHomeLink;
}

Map<String, dynamic> $MercariHomeLinkToJson(MercariHomeLink entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['feeDesc'] = entity.feeDesc;
	data['noviceStrategy'] = entity.noviceStrategy;
	data['shipmentFee'] = entity.shipmentFee;
	return data;
}

MercariHomeArticle $MercariHomeArticleFromJson(Map<String, dynamic> json) {
	final MercariHomeArticle mercariHomeArticle = MercariHomeArticle();
	final String? overview = jsonConvert.convert<String>(json['overview']);
	if (overview != null) {
		mercariHomeArticle.overview = overview;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		mercariHomeArticle.title = title;
	}
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		mercariHomeArticle.id = id;
	}
	final String? themeImage = jsonConvert.convert<String>(json['themeImage']);
	if (themeImage != null) {
		mercariHomeArticle.themeImage = themeImage;
	}
	final String? tag = jsonConvert.convert<String>(json['tag']);
	if (tag != null) {
		mercariHomeArticle.tag = tag;
	}
	return mercariHomeArticle;
}

Map<String, dynamic> $MercariHomeArticleToJson(MercariHomeArticle entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['overview'] = entity.overview;
	data['title'] = entity.title;
	data['id'] = entity.id;
	data['themeImage'] = entity.themeImage;
	data['tag'] = entity.tag;
	return data;
}

MercariHomeActivity $MercariHomeActivityFromJson(Map<String, dynamic> json) {
	final MercariHomeActivity mercariHomeActivity = MercariHomeActivity();
	final String? appImageUrl = jsonConvert.convert<String>(json['appImageUrl']);
	if (appImageUrl != null) {
		mercariHomeActivity.appImageUrl = appImageUrl;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		mercariHomeActivity.title = title;
	}
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		mercariHomeActivity.id = id;
	}
	final String? themeImageUrl = jsonConvert.convert<String>(json['themeImageUrl']);
	if (themeImageUrl != null) {
		mercariHomeActivity.themeImageUrl = themeImageUrl;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		mercariHomeActivity.url = url;
	}
	return mercariHomeActivity;
}

Map<String, dynamic> $MercariHomeActivityToJson(MercariHomeActivity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['appImageUrl'] = entity.appImageUrl;
	data['title'] = entity.title;
	data['id'] = entity.id;
	data['themeImageUrl'] = entity.themeImageUrl;
	data['url'] = entity.url;
	return data;
}

MercariHomeCat $MercariHomeCatFromJson(Map<String, dynamic> json) {
	final MercariHomeCat mercariHomeCat = MercariHomeCat();
	final String? name = jsonConvert.convert<String>(json['Name']);
	if (name != null) {
		mercariHomeCat.name = name;
	}
	final String? cat = jsonConvert.convert<String>(json['Cat']);
	if (cat != null) {
		mercariHomeCat.cat = cat;
	}
	final String? icon = jsonConvert.convert<String>(json['Icon']);
	if (icon != null) {
		mercariHomeCat.icon = icon;
	}
	final bool? hideHome = jsonConvert.convert<bool>(json['HideHome']);
	if (hideHome != null) {
		mercariHomeCat.hideHome = hideHome;
	}
	final dynamic xList = jsonConvert.convert<dynamic>(json['List']);
	if (xList != null) {
		mercariHomeCat.xList = xList;
	}
	return mercariHomeCat;
}

Map<String, dynamic> $MercariHomeCatToJson(MercariHomeCat entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['Name'] = entity.name;
	data['Cat'] = entity.cat;
	data['Icon'] = entity.icon;
	data['HideHome'] = entity.hideHome;
	data['List'] = entity.xList;
	return data;
}

MercariHomeHotGoods $MercariHomeHotGoodsFromJson(Map<String, dynamic> json) {
	final MercariHomeHotGoods mercariHomeHotGoods = MercariHomeHotGoods();
	final String? keyword = jsonConvert.convert<String>(json['Keyword']);
	if (keyword != null) {
		mercariHomeHotGoods.keyword = keyword;
	}
	final String? keywordSearch = jsonConvert.convert<String>(json['KeywordSearch']);
	if (keywordSearch != null) {
		mercariHomeHotGoods.keywordSearch = keywordSearch;
	}
	final String? desc = jsonConvert.convert<String>(json['Desc']);
	if (desc != null) {
		mercariHomeHotGoods.desc = desc;
	}
	final String? cat = jsonConvert.convert<String>(json['Cat']);
	if (cat != null) {
		mercariHomeHotGoods.cat = cat;
	}
	final List<MercariHomeHotGoodsList>? xList = (json['List'] as List?)?.map((e) => MercariHomeHotGoodsList.fromJson(e)).cast<MercariHomeHotGoodsList>().toList();
	if (xList != null) {
		mercariHomeHotGoods.xList = xList;
	}
	return mercariHomeHotGoods;
}

Map<String, dynamic> $MercariHomeHotGoodsToJson(MercariHomeHotGoods entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['Keyword'] = entity.keyword;
	data['KeywordSearch'] = entity.keywordSearch;
	data['Desc'] = entity.desc;
	data['Cat'] = entity.cat;
	data['List'] =  entity.xList?.map((v) => v.toJson()).toList();
	return data;
}

MercariHomeHotGoodsList $MercariHomeHotGoodsListFromJson(Map<String, dynamic> json) {
	final MercariHomeHotGoodsList mercariHomeHotGoodsList = MercariHomeHotGoodsList();
	final String? imageUrl = jsonConvert.convert<String>(json['ImageUrl']);
	if (imageUrl != null) {
		mercariHomeHotGoodsList.imageUrl = imageUrl;
	}
	final String? url = jsonConvert.convert<String>(json['Url']);
	if (url != null) {
		mercariHomeHotGoodsList.url = url;
	}
	final String? name = jsonConvert.convert<String>(json['Name']);
	if (name != null) {
		mercariHomeHotGoodsList.name = name;
	}
	final int? jPYPrice = jsonConvert.convert<int>(json['JPYPrice']);
	if (jPYPrice != null) {
		mercariHomeHotGoodsList.jPYPrice = jPYPrice;
	}
	return mercariHomeHotGoodsList;
}

Map<String, dynamic> $MercariHomeHotGoodsListToJson(MercariHomeHotGoodsList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['ImageUrl'] = entity.imageUrl;
	data['Url'] = entity.url;
	data['Name'] = entity.name;
	data['JPYPrice'] = entity.jPYPrice;
	return data;
}

MercariHomeSlider $MercariHomeSliderFromJson(Map<String, dynamic> json) {
	final MercariHomeSlider mercariHomeSlider = MercariHomeSlider();
	final String? slideImage = jsonConvert.convert<String>(json['slideImage']);
	if (slideImage != null) {
		mercariHomeSlider.slideImage = slideImage;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		mercariHomeSlider.title = title;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		mercariHomeSlider.url = url;
	}
	return mercariHomeSlider;
}

Map<String, dynamic> $MercariHomeSliderToJson(MercariHomeSlider entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['slideImage'] = entity.slideImage;
	data['title'] = entity.title;
	data['url'] = entity.url;
	return data;
}