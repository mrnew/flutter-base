import 'package:get/get.dart';
import 'package:mrnew_libary/widget/select_dialog.dart';
import 'package:package_info/package_info.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'state.dart';

class AboutLogic extends BaseLogic {
  final AboutState data = AboutState();

  @override
  void onReady() {
    super.onReady();
    PackageInfo.fromPlatform()
        .then((value) => data.version.value = value.version);
  }

  void onChange() async {
    if (!GlobalConfig.isTest) return;
    var value =
        await SelectDialog.showModal(Get.context!, items: ['测试平台', '正式平台']);
    if (value == null) return;
    //注销
    await UiUtils.logout(Get.context!);
    await CacheManager.getInstance()
        .put(false, 'isTest', (value == 0).toString());
    await HttpClient.instance.restart();
  }
}
