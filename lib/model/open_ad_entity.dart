import 'package:wameiji/generated/json/open_ad_entity.g.dart';

import 'package:wameiji/generated/json/base/json_field.dart';

@JsonSerializable()
class OpenAdEntity {

	OpenAdEntity();

	factory OpenAdEntity.fromJson(Map<String, dynamic> json) => $OpenAdEntityFromJson(json);

	Map<String, dynamic> toJson() => $OpenAdEntityToJson(this);

	@JSONField(name: 'Img')
	String? img;
	@JSONField(name: 'WaitSecond')
	int? waitSecond;
	@JSONField(name: 'Open')
	bool? open;
	@JSONField(name: 'Url')
	String? url;
}
