import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/user_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/event_bus.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import '../../../config.dart';
import 'state.dart';

class LoginSysLogic extends BaseLogic {
  final LoginSysState data = LoginSysState();

  @override
  void onInit() async {
    super.onInit();
    var lastName = await CacheManager.getInstance().get(false, 'lastName', '');
    data.unameController.text = lastName!;
    data.unameController.selection = TextSelection.fromPosition(TextPosition(
        affinity: TextAffinity.downstream, offset: lastName.length));
  }

  void onLogin(BuildContext context) {
    LibUtils.hideKeyboard(context);
    if (!data.isArgree.value) {
      LibUtils.showToast("确认阅读并同意用户条款和隐私政策");
      return;
    }
    var name = data.unameController.text;
    var pwd = data.pwdController.text;
    if (name.isEmpty) {
      LibUtils.showToast("请输入账号");
      return;
    }
    if (pwd.isEmpty) {
      LibUtils.showToast("请输入密码");
      return;
    }
    UserEntity user = UserEntity();
    user.localId = name;
    if (!UiUtils.isEmpty(name)) {
      CacheManager.getInstance().put(false, 'lastName', name);
    }
    UserManager.save(user);
    LibUtils.eventBus.fire(const LoginEvent());
    LibUtils.showToast("登录成功");
    LibUtils.hideLoading();
    if (GlobalConfig.isMustLogin) {
      Get.offNamed('main_page');
    } else {
      Get.back();
    }
    return;
    HttpClient.instance.request(
      {
        'n': 'Sig.Front.AppApi.LoginAction',
        'user': name,
        'password': pwd,
      },
      isGet: false,
      cancelToken: cancelToken,
      isShowLoading: true,
      isSuccessHideLoading: false,
      success: (Object? data) {
        getInfo(name, false);
      },
    );
  }

  void getInfo(String name, bool isQuick) {
    HttpClient.instance.request<UserEntity>({
      'n': 'Sig.Front.User.AppUserSetting.GetUserInfo',
    }, isGet: false, cancelToken: cancelToken, successText: "登录成功",
        success: (UserEntity? data) {
      if (data?.hasCompleteInfo ?? true) {
        data!.localId = name;
        if (!UiUtils.isEmpty(name)) {
          CacheManager.getInstance().put(false, 'lastName', name);
        }
        UserManager.save(data);
        LibUtils.eventBus.fire(const LoginEvent());
        LibUtils.hideLoading();
        if (GlobalConfig.isMustLogin) {
          Get.offNamed('main_page');
        } else {
          Get.back();
        }
      } else {
        LibUtils.hideLoading();
        Get.offNamed('update_info_page',
            arguments: name, preventDuplicates: false);
      }
    }, error: (HttpException e) {
      LibUtils.hideLoading();
    });
  }
}
