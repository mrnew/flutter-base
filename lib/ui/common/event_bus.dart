import 'package:flutter/widgets.dart';

class GlobalConfigRefresh {
  const GlobalConfigRefresh();
}

class AppLifecycleChange {
  AppLifecycleState state;

  AppLifecycleChange(this.state);
}

///订单发生改变
class OrderChangeEvent {
  const OrderChangeEvent();
}

class GoFirstOrderTabEvent {
  const GoFirstOrderTabEvent();
}

class UserEvent {
  const UserEvent();
}

///进入应用获取到用户数据
class InitUserEvent {
  const InitUserEvent();
}

///登录
class LoginEvent {
  const LoginEvent();
}

///注销
class LogoutEvent {
  const LogoutEvent();
}

///购物车发生改变
class CartChangeEvent {
  const CartChangeEvent();
}

///购物车数量发生改变
class CartNumChangeEvent {
  const CartNumChangeEvent();
}

///收藏商品发生改变
class FavGoodsChangeEvent {
  const FavGoodsChangeEvent();
}

class FavSalerChangeEvent {
  const FavSalerChangeEvent();
}

///收藏话题发生改变
class FavNewsChangeEvent {
  const FavNewsChangeEvent();
}

///雅虎关注发生改变
class AttentionChangeEvent {
  const AttentionChangeEvent();
}

///雅虎关注编辑发生改变
class YahooRecentViewEvent {
  const YahooRecentViewEvent();
}

class AttentionGoodsRefrshEvent {
  const AttentionGoodsRefrshEvent();
}

class AttentionSellerRefrshEvent {
  const AttentionSellerRefrshEvent();
}

///雅虎订单变化
class YahooOrderChangeEvent {
  const YahooOrderChangeEvent();
}

class YahooOrderDelayRefreshEvent {
  const YahooOrderDelayRefreshEvent();
}

///雅虎未读订单变化
class YahooOrderUnreadChangeEvent {
  const YahooOrderUnreadChangeEvent();
}

///跳转活动折扣
class GoEventList {
  const GoEventList();
}

///跳转种草推荐
class GoFindRecommdList {
  const GoFindRecommdList();
}

///跳转购物体验
class GoFindExpList {
  const GoFindExpList();
}

///个人中心页面
class MainPageVisableEvent {
  const MainPageVisableEvent();
}

class YahooMainPageVisableEvent {
  const YahooMainPageVisableEvent();
}

class MessageGroupVisableEvent {
  const MessageGroupVisableEvent();
}

class MainPageUnvisableEvent {
  const MainPageUnvisableEvent();
}

class ShowFindEvent {
  const ShowFindEvent();
}

///跳转亚马逊分类
class GoAmazonCategory {
  String name;

  GoAmazonCategory(this.name);
}

class GoZozoBrand {
  const GoZozoBrand();
}

class HistoryChange {
  const HistoryChange();
}

class OrderOther {
  final String status;

  const OrderOther(this.status);
}

class CouponUseEvent {
  const CouponUseEvent();
}

class CouponExchangeEvent {
  const CouponExchangeEvent();
}

class SignEvent {
  const SignEvent();
}

class ShareInviteEvent {
  final int type;

  const ShareInviteEvent(this.type);
}

class ShieldingEvent {
  const ShieldingEvent();
}
