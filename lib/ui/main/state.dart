import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:wameiji/model/open_ad_entity.dart';
import 'package:wameiji/ui/base/page.dart';

class MainState with BaseStateMixin {
  RxInt tabIndex = 0.obs;
  static int? pushType;
  static String? pushParam;
  static OpenAdEntity? entity;
  GlobalKey globalKey = GlobalKey();

  MainState();
}
