import 'package:get/get.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:flutter/material.dart';

class GlobalLoading extends StatelessWidget {
  const GlobalLoading();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: const <Widget>[
          CircularProgressIndicator(),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Text(
              "加载中",
              style: MyText.textStyle14_878787,
            ),
          ),
        ],
      ),
    );
  }
}

class GlobalError extends StatelessWidget {
  const GlobalError();

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Image(image: AssetImage(Res.error_data_icon)),
    );
  }
}

class GlobalEmty extends StatelessWidget {
  const GlobalEmty();

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Image(image: AssetImage(Res.empty_data_icon)),
    );
  }
}

class GlobalUnsupport extends StatelessWidget {
  const GlobalUnsupport();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Image(image: AssetImage(Res.unsupport_icon)), // 暂不支持购买此商品
          const Padding(
            padding: EdgeInsets.only(top: 20, bottom: 26),
            child: Text(
              "暂不支持购买此商品",
              style: TextStyle(
                  color: Color(0xffc0c4cf),
                  fontWeight: FontWeight.w500,
                  fontSize: 16.0),
            ),
          ), // 矩形
          GestureDetector(
            onTap: () {
              Get.back();
            },
            behavior: HitTestBehavior.translucent,
            child: Container(
              width: 145,
              height: 37,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(18.5)),
                  color: Color(0xffa6abb7)),
              alignment: Alignment.center,
              child: // 返回
              const Text(
                "返回",
                style: TextStyle(
                    color: Color(0xffffffff),
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0),
              ),
            ),
          )
        ],
      ),
    );
  }
}

void showMyCustomLoadingDialog(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return const MyCustomLoadingDialog();
      });
}

class MyCustomLoadingDialog extends StatelessWidget {
  const MyCustomLoadingDialog();

  @override
  Widget build(BuildContext context) {
    Duration insetAnimationDuration = const Duration(milliseconds: 100);
    Curve insetAnimationCurve = Curves.decelerate;

    RoundedRectangleBorder _defaultDialogShape = const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(2.0)));

    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets +
          const EdgeInsets.symmetric(horizontal: 40.0, vertical: 24.0),
      duration: insetAnimationDuration,
      curve: insetAnimationCurve,
      child: MediaQuery.removeViewInsets(
        removeLeft: true,
        removeTop: true,
        removeRight: true,
        removeBottom: true,
        context: context,
        child: Center(
          child: SizedBox(
            width: 120,
            height: 120,
            child: Material(
              elevation: 24.0,
              color: Theme.of(context).dialogBackgroundColor,
              type: MaterialType.card,
              //在这里修改成我们想要显示的widget就行了，外部的属性跟其他Dialog保持一致
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Text("加载中"),
                  ),
                ],
              ),
              shape: _defaultDialogShape,
            ),
          ),
        ),
      ),
    );
  }
}
