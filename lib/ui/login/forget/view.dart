import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:mrnew_libary/widget/flutter_countdown.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:wameiji/ui/widget/business/global_action_btn.dart';

import 'logic.dart';
import 'state.dart';

class ForgetPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<ForgetPage, ForgetLogic, ForgetState> {
  @override
  ForgetLogic createLogic() {
    return ForgetLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(
      context,
      logic,
      () {
        return ListView(
            padding: const EdgeInsets.fromLTRB(15, 30, 15, 20),
            children: <Widget>[
              _getPhone(logic, state),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: GlobalActionBtn(
                  title: '确认',
                  onPressed: () {
                    logic.onSubmit();
                  },
                ),
              ),
            ]);
      },
      title: '找回密码',
      elevation: 0,
    );
  }
}

_getPhone(ForgetLogic logic, ForgetState state) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      const Padding(
        padding: EdgeInsets.only(bottom: 30),
        child: Text(
          "找回密码",
          style: TextStyle(
              color: Color(0xff333333),
              fontWeight: FontWeight.w500,
              fontSize: 24.0),
        ),
      ),
      Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(6)),
              color: Color(0xffffffff)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 75,
                    child: Text("手机号码",
                        style: TextStyle(
                            color: Color(0xff333333), fontSize: 14.0)),
                  ),
                  Expanded(
                    child: TextField(
                      controller: state.phoneController,
                      style: const TextStyle(
                          color: Color(0xff333333), fontSize: 14.0),
                      decoration: const InputDecoration(
                        hintStyle: TextStyle(
                            color: Color(0xff999999), fontSize: 14.0),
                        hintText: "请输入手机号码",
                        contentPadding: EdgeInsets.symmetric(vertical: 15),
                        border: OutlineInputBorder(borderSide: BorderSide.none),
                      ),
                      inputFormatters: [LengthLimitingTextInputFormatter(11)],
                      keyboardType: TextInputType.phone,
                    ),
                  ),
                ],
              ),
              MyStyle.divider,
              Row(
                children: [
                  const SizedBox(
                    width: 75,
                    child: Text("验证码",
                        style: TextStyle(
                            color: Color(0xff333333), fontSize: 14.0)),
                  ),
                  Expanded(
                    child: TextField(
                      controller: state.codeController,
                      style: const TextStyle(
                          color: Color(0xff333333), fontSize: 14.0),
                      decoration: const InputDecoration(
                        hintStyle: TextStyle(
                            color: Color(0xff999999), fontSize: 14.0),
                        hintText: "请输入验证码",
                        contentPadding: EdgeInsets.symmetric(vertical: 15),
                        border: OutlineInputBorder(borderSide: BorderSide.none),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    behavior: HitTestBehavior.translucent,
                    child: CountDown(
                      renderSemanticLabel: (count) {
                        if (count == 60) {
                          return "发送验证码";
                        }
                        return "$count 秒后重试";
                      },
                      beginCount: 60,
                      endCount: 0,
                      textStyle: const TextStyle(
                          color: Color(0xff333333), fontSize: 14.0),
                      refs: (ctr) {
                        state.ctr = ctr;
                      },
                      statusListener: (status) {},
                      onPress: (ctr) {
                        if (ctr.isAnimating) {
                          return Future.value(false);
                        }
                        logic.onCode();
                        return Future.value(true);
                      },
                    ),
                  )
                ],
              ),
              MyStyle.divider,
              Obx(() {
                return Row(
                  children: [
                    const SizedBox(
                      width: 75,
                      child: Text("新密码",
                          style: TextStyle(
                              color: Color(0xff333333), fontSize: 14.0)),
                    ),
                    Expanded(
                      child: TextField(
                        controller: state.pwdController,
                        style: const TextStyle(
                            color: Color(0xff333333), fontSize: 14.0),
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(
                              color: Color(0xff999999), fontSize: 14.0),
                          hintText: "请设置新密码",
                          contentPadding: EdgeInsets.symmetric(vertical: 15),
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                        ),
                        obscureText: !state.hasVisibility.value,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        state.hasVisibility.value = !state.hasVisibility.value;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Icon(
                        state.hasVisibility.value
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: const Color(0xff333333),
                        size: 18,
                      ),
                    )
                  ],
                );
              }),
              MyStyle.divider,
              Obx(() {
                return Row(
                  children: [
                    const SizedBox(
                      width: 75,
                      child: Text("确认密码",
                          style: TextStyle(
                              color: Color(0xff333333), fontSize: 14.0)),
                    ),
                    Expanded(
                      child: TextField(
                        controller: state.pwdSureController,
                        style: const TextStyle(
                            color: Color(0xff333333), fontSize: 14.0),
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(
                              color: Color(0xff999999), fontSize: 14.0),
                          hintText: "请再次设置新密码",
                          contentPadding: EdgeInsets.symmetric(vertical: 15),
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                        ),
                        obscureText: !state.hasVisibility1.value,
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        state.hasVisibility1.value =
                            !state.hasVisibility1.value;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Icon(
                        state.hasVisibility1.value
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: const Color(0xff333333),
                        size: 18,
                      ),
                    )
                  ],
                );
              }),
            ],
          )),
    ],
  );
}
