import 'package:flutter/material.dart';
import 'package:mrnew_libary/widget/divider.dart';

class MyColor {
  static const Color globalBg = Color(0xfff2f2f2);
  static const Color gray_23 = Color(0xff232323);
  static const Color gray_f7 = Color(0xfff7f7f7);
  static const Color gray_e6 = Color(0xffe6e6e6);
  static const Color imageLoadingColor = Color(0xfff7f7f7);

  static const Color globalThemeColor = Color(0xff18b752);
  static const Color disabledColor = Color(0xffaaaaaa);
  static const Color globalBtnColor = Color(0xff18b752);
}

class MyText {
  static const TextStyle textStyle11_b3 =
      TextStyle(fontSize: 11, color: Color(0xffb3b3b3));
  static const TextStyle textStyle11_ccc =
      TextStyle(fontSize: 11, color: Color(0xffcccccc));
  static const TextStyle textStyle11_87 =
      TextStyle(fontSize: 11, color: Color(0xff878787));
  static const TextStyle textStyle11_fff =
      TextStyle(fontSize: 11, color: Color(0xffffffff));
  static const TextStyle textStyle11_23 =
      TextStyle(fontSize: 11, color: Color(0xff232323));

  static const TextStyle textStyle12_23 =
      TextStyle(fontSize: 12, color: Color(0xff232323));
  static const TextStyle textStyle12_f86072 =
      TextStyle(fontSize: 12, color: Color(0xfff86072));
  static const TextStyle textStyle12_theme =
      TextStyle(fontSize: 12, color: MyColor.globalThemeColor);
  static const TextStyle textStyle12_c3 =
      TextStyle(fontSize: 12, color: Color(0xffc3c3c3));
  static const TextStyle textStyle12_b3 =
      TextStyle(fontSize: 12, color: Color(0xffb3b3b3));
  static const TextStyle textStyle12_999 =
      TextStyle(fontSize: 12, color: Color(0xff999999));
  static const TextStyle textStyle12_87 =
      TextStyle(fontSize: 12, color: Color(0xff878787));
  static const TextStyle textStyle12_333 =
      TextStyle(fontSize: 12, color: Color(0xff333333));
  static const TextStyle textStyle12_666 =
      TextStyle(fontSize: 12, color: Color(0xff666666));
  static const TextStyle textStyle12_37A3EE =
      TextStyle(fontSize: 12, color: Color(0xff37A3EE));
  static const TextStyle textStyle12_fff =
      TextStyle(fontSize: 12, color: Color(0xffffffff));
  static const TextStyle textStyle12_000 =
      TextStyle(fontSize: 12, color: Color(0xff000000));

  static const TextStyle textStyle13_777 =
      TextStyle(fontSize: 13, color: Color(0xff777777));
  static const TextStyle textStyle13_000 =
      TextStyle(fontSize: 13, color: Color(0xff000000));
  static const TextStyle textStyle13_333 =
      TextStyle(fontSize: 13, color: Color(0xff333333));
  static const TextStyle textStyle13_fff =
      TextStyle(fontSize: 13, color: Color(0xffffffff));
  static const TextStyle textStyle13_9b =
      TextStyle(fontSize: 13, color: Color(0xff9b9b9b));
  static const TextStyle textStyle13_999 =
      TextStyle(fontSize: 13, color: Color(0xff999999));
  static const TextStyle textStyle13_f86072 =
      TextStyle(fontSize: 13, color: Color(0xfff86072));
  static const TextStyle textStyle13_theme =
      TextStyle(fontSize: 13, color: MyColor.globalThemeColor);
  static const TextStyle textStyle13_b3 =
      TextStyle(fontSize: 13, color: Color(0xffb3b3b3));

  static const TextStyle textStyle14_b3 =
      TextStyle(fontSize: 14, color: Color(0xffb3b3b3));
  static const TextStyle textStyle14_9b =
      TextStyle(fontSize: 14, color: Color(0xff9b9b9b));
  static const TextStyle textStyle14_202228 =
      TextStyle(fontSize: 14, color: Color(0xff202228));
  static const TextStyle textStyle14_7b =
      TextStyle(fontSize: 14, color: Color(0xff7b7b7b));
  static const TextStyle textStyle14_f86072 =
      TextStyle(fontSize: 14, color: Color(0xfff86072));
  static const TextStyle textStyle14_theme =
      TextStyle(fontSize: 14, color: MyColor.globalThemeColor);
  static const TextStyle textStyle14_878787 =
      TextStyle(fontSize: 14, color: Color(0xff878787));
  static const TextStyle textStyle14_333 =
      TextStyle(fontSize: 14, color: Color(0xff333333));
  static const TextStyle textStyle14_0088ff =
      TextStyle(fontSize: 14, color: Color(0xff0088ff));
  static const TextStyle textStyle14_000 =
      TextStyle(fontSize: 14, color: Color(0xff000000));
  static const TextStyle textStyle14_23 =
      TextStyle(fontSize: 14, color: Color(0xff232323));
  static const TextStyle textStyle14_fff =
      TextStyle(fontSize: 14, color: Color(0xffffffff));
  static const TextStyle textStyle14_5e616b =
      TextStyle(fontSize: 14, color: Color(0xff5e616b));

  static const TextStyle textStyle15_333 =
      TextStyle(fontSize: 15, color: Color(0xff333333));
  static const TextStyle textStyle15_f86072 =
      TextStyle(fontSize: 15, color: Color(0xfff86072));
  static const TextStyle textStyle15_theme =
      TextStyle(fontSize: 15, color: MyColor.globalThemeColor);
  static const TextStyle textStyle15_fff =
      TextStyle(fontSize: 15, color: Colors.white);

  static const TextStyle textStyle16_333 =
      TextStyle(fontSize: 16, color: Color(0xff333333));
  static const TextStyle textStyle16_fff =
      TextStyle(fontSize: 16, color: Colors.white);
  static const TextStyle textStyle16_777 =
      TextStyle(fontSize: 16, color: Color(0xff777777));
  static const TextStyle textStyle16_666 =
      TextStyle(fontSize: 16, color: Color(0xff666666));

  static const TextStyle textStyle17_333 =
      TextStyle(fontSize: 17, color: Color(0xff333333));

  static const TextStyle textStyle18_333 =
      TextStyle(fontSize: 18, color: Color(0xff333333));

  static const TextStyle textStyle18_fff =
      TextStyle(fontSize: 18, color: Color(0xffffffff));

  static const TextStyle textStyle20_fff =
      TextStyle(fontSize: 20, color: Color(0xffffffff));
  static const TextStyle textStyle20_f86072 =
      TextStyle(fontSize: 20, color: Color(0xfff86072));
  static const TextStyle textStyle20_theme =
      TextStyle(fontSize: 20, color: MyColor.globalThemeColor);

  static const TextStyle textStyle22_fff =
      TextStyle(fontSize: 22, color: Color(0xffffffff));
}

class MyStyle {
  static const MyDivider divider = MyDivider();
  static const BoxDecoration boxDecoration_white_shadow = BoxDecoration(
    boxShadow: [
      BoxShadow(
          color: Color(0x0d000000),
          offset: Offset(0, 3),
          blurRadius: 5,
          spreadRadius: 0)
    ],
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(10)),
  );
  static const BoxDecoration boxDecoration_white = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(7.5)),
  );
  static const RoundedRectangleBorder grayBorder = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(7.5)),
      side: BorderSide(
          width: 0.5, color: Color(0xffd3d3d3), style: BorderStyle.solid));

  static const BoxDecoration tagBoxDecoration = BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(3)),
    color: Color(0xfff1f3f6),
  );

  static const BoxDecoration tagBoxDecoration1 = BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(4)),
      border: Border.symmetric(
          horizontal: BorderSide(
              width: 1, color: Color(0xffE87284), style: BorderStyle.solid),
          vertical: BorderSide(
              width: 1, color: Color(0xffE87284), style: BorderStyle.solid)));
  static const RoundedRectangleBorder selGrayBorder = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(7.5)),
      side: BorderSide(
          width: 1, color: Color(0xffd3d3d3), style: BorderStyle.solid));
  static const RoundedRectangleBorder selRedBorder = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(7.5)),
      side: BorderSide(
          width: 1, color: Color(0xfff86072), style: BorderStyle.solid));
}
