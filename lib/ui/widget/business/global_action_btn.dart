import 'package:flutter/material.dart';
import 'package:wameiji/ui/common/common_res.dart';

class GlobalActionBtn extends StatelessWidget {
  final EdgeInsets? margin;
  final String? title;
  final bool hasShadow;
  final Color shadowColor;
  final Color bgColor;
  final Color textColor;
  final VoidCallback? onPressed;

  const GlobalActionBtn({
    this.margin,
    this.title,
    this.onPressed,
    this.hasShadow = false,
    this.shadowColor = const Color(0xff18b752),
    this.bgColor = MyColor.globalBtnColor,
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      height: 45,
      decoration: !hasShadow
          ? null
          : BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: shadowColor,
                    offset: const Offset(0, 3),
                    blurRadius: 15,
                    spreadRadius: 0)
              ],
              borderRadius: const BorderRadius.all(Radius.circular(6)),
            ),
      child: TextButton(
          onPressed: onPressed,
          style: TextButton.styleFrom(
              backgroundColor: bgColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6))),
          child: Text(
            title ?? '',
            style: TextStyle(
              fontSize: 16,
              color: textColor,
              fontWeight: FontWeight.w600,
            ),
          )),
    );
  }
}
