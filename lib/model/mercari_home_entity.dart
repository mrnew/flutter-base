import 'package:wameiji/generated/json/mercari_home_entity.g.dart';

import 'package:wameiji/generated/json/base/json_field.dart';

@JsonSerializable()
class MercariHomeEntity {

	MercariHomeEntity();

	factory MercariHomeEntity.fromJson(Map<String, dynamic> json) => $MercariHomeEntityFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeEntityToJson(this);

  List<MercariHomeActivity>? activity;
  List<MercariHomeActivity>? activitySmall;
  MercariHomeAnnouncement? announcement;
  MercariHomeLink? link;
  List<MercariHomeCat>? cat;
  List<MercariHomeHotGoods>? hotGoods;
  List<MercariHomeSlider>? slider;
  List<MercariHomeArticle>? article;
}

@JsonSerializable()
class MercariHomeAnnouncement {

	MercariHomeAnnouncement();

	factory MercariHomeAnnouncement.fromJson(Map<String, dynamic> json) => $MercariHomeAnnouncementFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeAnnouncementToJson(this);

  @JSONField(name: "ContentPlain")
  String? contentPlain;
  @JSONField(name: "Id")
  String? id;
  @JSONField(name: "Title")
  String? title;
}

@JsonSerializable()
class MercariHomeLink {

	MercariHomeLink();

	factory MercariHomeLink.fromJson(Map<String, dynamic> json) => $MercariHomeLinkFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeLinkToJson(this);

  String? feeDesc;
  String? noviceStrategy;
  String? shipmentFee;
}

@JsonSerializable()
class MercariHomeArticle {

	MercariHomeArticle();

	factory MercariHomeArticle.fromJson(Map<String, dynamic> json) => $MercariHomeArticleFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeArticleToJson(this);

  String? overview;
  String? title;
  String? id;
  String? themeImage;
  String? tag;
}

@JsonSerializable()
class MercariHomeActivity {

	MercariHomeActivity();

	factory MercariHomeActivity.fromJson(Map<String, dynamic> json) => $MercariHomeActivityFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeActivityToJson(this);

  String? appImageUrl;
  String? title;
  String? id;
  String? themeImageUrl;
  String? url;
}

@JsonSerializable()
class MercariHomeCat {

	MercariHomeCat();

	factory MercariHomeCat.fromJson(Map<String, dynamic> json) => $MercariHomeCatFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeCatToJson(this);

  @JSONField(name: "Name")
  String? name;
  @JSONField(name: "Cat")
  String? cat;
  @JSONField(name: "Icon")
  String? icon;
  @JSONField(name: "HideHome")
  bool? hideHome;
  @JSONField(name: "List")
  dynamic xList;
}

@JsonSerializable()
class MercariHomeHotGoods {

	MercariHomeHotGoods();

	factory MercariHomeHotGoods.fromJson(Map<String, dynamic> json) => $MercariHomeHotGoodsFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeHotGoodsToJson(this);

  @JSONField(name: "Keyword")
  String? keyword;
  @JSONField(name: "KeywordSearch")
  String? keywordSearch;
  @JSONField(name: "Desc")
  String? desc;
  @JSONField(name: "Cat")
  String? cat;
  @JSONField(name: "List")
  List<MercariHomeHotGoodsList>? xList;
}

@JsonSerializable()
class MercariHomeHotGoodsList {

	MercariHomeHotGoodsList();

	factory MercariHomeHotGoodsList.fromJson(Map<String, dynamic> json) => $MercariHomeHotGoodsListFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeHotGoodsListToJson(this);

  @JSONField(name: "ImageUrl")
  String? imageUrl;
  @JSONField(name: "Url")
  String? url;
  @JSONField(name: "Name")
  String? name;
  @JSONField(name: "JPYPrice")
  int? jPYPrice;
}

@JsonSerializable()
class MercariHomeSlider {

	MercariHomeSlider();

	factory MercariHomeSlider.fromJson(Map<String, dynamic> json) => $MercariHomeSliderFromJson(json);

	Map<String, dynamic> toJson() => $MercariHomeSliderToJson(this);

  String? slideImage;
  String? title;
  String? url;
}
