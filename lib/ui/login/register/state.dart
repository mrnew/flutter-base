import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wameiji/ui/base/page.dart';

class RegisterState with BaseStateMixin {
  AnimationController? ctr;

  TextEditingController phoneController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  TextEditingController pwdController = TextEditingController();
  TextEditingController pwdSureController = TextEditingController();
  RxBool hasVisibility = false.obs;
  RxBool hasVisibility1 = false.obs;
}
