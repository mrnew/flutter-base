import 'package:get/get.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/util/page_utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'state.dart';

class WebLogic extends BaseLogic {
  final WebState data = WebState();

  @override
  void onInit() async {
    super.onInit();
    Map map = Get.arguments;
    data.url = map['url'];
    data.title = map['title'];
    data.progressController.start();
    data.progressController.start();
    data.webController.setNavigationDelegate(
      getCommonNavigationDelegate(data),
    );
    data.webController.setJavaScriptMode(JavaScriptMode.unrestricted);
  }

  @override
  void onReady() {
    super.onReady();
    data.webController.loadRequest(Uri.parse(data.url ?? ''));
  }

  @override
  void onClose() {
    data.jsBridge.dispose();
    super.onClose();
  }
}

NavigationDelegate getCommonNavigationDelegate(WebState data,
    {ExtandJsBridgeCallBack? extandJs, Function? onPageFinished}) {
  return NavigationDelegate(
    onPageStarted: (String url) {
      data.jsBridge.loadJs(data.webController);
      PageUtils.initJs(data.jsBridge, Get.context!, extandJs: extandJs);
    },
    onPageFinished: (String url) async {
      data.jsBridge.init();
      data.progressController.end();
      onPageFinished?.call(url);
    },
    onNavigationRequest: (NavigationRequest request) {
      if (!data.jsBridge.handlerUrl(request.url)) {
        //jsbridge 信息
        return NavigationDecision.prevent;
      }
      if (request.url.contains('item.taobao.com/item.htm')) {
        //跳转淘宝
        var url = request.url.replaceFirst('http://', 'taobao://');
        url = url.replaceFirst('https://', 'taobao://');
        PageUtils.startApp(url, 1);
        return NavigationDecision.prevent;
      }
      if (request.url.startsWith("alipays://")) {
        PageUtils.startApp(request.url, 0);
        return NavigationDecision.prevent;
      }
      if (request.url.startsWith("taobao://")) {
        PageUtils.startApp(request.url, 1);
        return NavigationDecision.prevent;
      }
      if (request.url != 'https://__bridge_loaded__/') {
        data.progressController.start();
      }
      return NavigationDecision.navigate;
    },
  );
}
