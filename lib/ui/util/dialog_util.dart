import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/ui/util/page_utils.dart';

import '../common/common_res.dart';

class DialogUtil {
  ///通用提示弹出框
  static Future<int?> showMyDialog(String text,
      {String title = '提示',
      String positiveText = '确认',
      String negativeText = '取消',
      bool hasNegative = true}) async {
    var actions = <Widget>[];
    if (hasNegative) {
      actions.add(CupertinoDialogAction(
        child: Text(negativeText),
        onPressed: () {
          Get.back(result: 0);
        },
      ));
    }
    actions.add(CupertinoDialogAction(
      child: Text(positiveText),
      onPressed: () {
        Get.back(result: 1);
      },
    ));
    return showCupertinoDialog<int>(
      context: Get.context!,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(text),
              ],
            ),
          ),
          actions: actions,
        );
      },
    );
  }

  ///通用输入框弹出框
  static Future<String?> showMyEditDialog(TextEditingController controller,
      {String title = '请输入',
      String positiveText = '确认',
      String negativeText = '取消',
      bool hasNegative = true,
      int? maxLength}) async {
    var actions = <Widget>[];
    if (hasNegative) {
      actions.add(CupertinoDialogAction(
        child: Text(negativeText),
        onPressed: () {
          Get.back();
        },
      ));
    }
    actions.add(CupertinoDialogAction(
      child: Text(positiveText),
      onPressed: () {
        Get.back(result: controller.text);
      },
    ));

    return showCupertinoDialog<String>(
      context: Get.context!,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(title),
          content: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: CupertinoTextField(
              controller: controller,
              maxLength: maxLength,
            ),
          ),
          actions: actions,
        );
      },
    );
  }

  ///用户协议弹出框
  static Future<int?> showUserNotceDialog(BuildContext context) async {
    return showDialog<int>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext dialogContext) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 400),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  decoration: const BoxDecoration(
                      color: Color(0xffd55b74),
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(10))),
                  alignment: Alignment.center,
                  height: 45,
                  child: const Text(
                    '服务协议和隐私政策',
                    style: TextStyle(
                        color: Color(0xffffffff),
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0),
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                  color: Colors.white,
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const TextSpan(
                          text:
                              '请你务必审慎阅读、充分理解“用户协议”和“隐私政策”各条款，包括但不限于：为了向你提供日淘、运输、内容分享等服务，我们需要收集并保管你的姓名、电话、地址等信息。\n你可阅读',
                          style: MyText.textStyle14_333,
                        ),
                        TextSpan(
                            text: '《用户协议》',
                            style: const TextStyle(
                                fontSize: 14, color: Colors.red),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                PageUtils.startWeb(
                                    GlobalConfig.userProtocolUrl, '用户条款');
                              }),
                        const TextSpan(
                          text: '和',
                          style: MyText.textStyle14_333,
                        ),
                        TextSpan(
                            text: '《隐私政策》',
                            style: const TextStyle(
                                fontSize: 14, color: Colors.red),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                PageUtils.startWeb(
                                    GlobalConfig.userPrivacyUrl, '隐私政策');
                              }),
                        const TextSpan(
                          text: '了解详细内容。如你同意，请点击“同意”开始接受我们的服务。',
                          style: MyText.textStyle14_333,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(10))),
                  padding: const EdgeInsets.only(top: 10, bottom: 15),
                  child: Wrap(
                    children: <Widget>[
                      SizedBox(
                        width: 100,
                        height: 35,
                        child: TextButton(
                          child: const Text(
                            '暂不使用',
                            style: MyText.textStyle14_fff,
                          ),
                          style: TextButton.styleFrom(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            backgroundColor: const Color(0xffa6abb7),
                            padding: EdgeInsets.zero,
                          ),
                          onPressed: () {
                            Get.back(result: 0);
                          },
                        ),
                      ),
                      Container(
                        width: 100,
                        height: 35,
                        margin: const EdgeInsets.only(left: 40),
                        child: TextButton(
                          child: const Text(
                            '同意',
                            style: MyText.textStyle14_fff,
                          ),
                          style: TextButton.styleFrom(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            backgroundColor: const Color(0xffd55b74),
                            padding: EdgeInsets.zero,
                          ),
                          onPressed: () {
                            Get.back(result: 1);
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
