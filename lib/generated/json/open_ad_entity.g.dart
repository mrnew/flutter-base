import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/open_ad_entity.dart';

OpenAdEntity $OpenAdEntityFromJson(Map<String, dynamic> json) {
	final OpenAdEntity openAdEntity = OpenAdEntity();
	final String? img = jsonConvert.convert<String>(json['Img']);
	if (img != null) {
		openAdEntity.img = img;
	}
	final int? waitSecond = jsonConvert.convert<int>(json['WaitSecond']);
	if (waitSecond != null) {
		openAdEntity.waitSecond = waitSecond;
	}
	final bool? open = jsonConvert.convert<bool>(json['Open']);
	if (open != null) {
		openAdEntity.open = open;
	}
	final String? url = jsonConvert.convert<String>(json['Url']);
	if (url != null) {
		openAdEntity.url = url;
	}
	return openAdEntity;
}

Map<String, dynamic> $OpenAdEntityToJson(OpenAdEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['Img'] = entity.img;
	data['WaitSecond'] = entity.waitSecond;
	data['Open'] = entity.open;
	data['Url'] = entity.url;
	return data;
}