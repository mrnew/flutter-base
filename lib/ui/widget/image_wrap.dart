import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ImageWrap extends StatelessWidget {
  final List<String>? imgList;
  final int crossAxisCount;
  final bool isRight;

  ///是否是缩略图，如果是的话，点击放大时会去掉URL的后缀
  final bool isThumbnail;

  const ImageWrap(this.imgList,
      {this.crossAxisCount = 4,
      this.isRight = false,
      this.isThumbnail = false});

  @override
  Widget build(BuildContext context) {
    List<String> img;
    if (imgList == null) {
      img = [];
    } else {
      img = imgList!;
    }
    var rowCount = (img.length / crossAxisCount).ceil();
    var list = <Widget>[];
    for (var i = 0; i < rowCount; i++) {
      var list1 = <Widget>[];
      if (i != 0) {
        list.add(const SizedBox(
          height: 4,
        ));
      }
      for (var j = 0; j < crossAxisCount; j++) {
        var index = i * crossAxisCount + j;
        if (j != 0) {
          list1.add(const SizedBox(
            width: 4,
          ));
        }
        var realIndex = index;
        var item;
        var yu = img.length % crossAxisCount;
        if (yu != 0 && i == rowCount - 1 && isRight) {
          if (j < crossAxisCount - yu) {
            item = null;
          } else {
            realIndex = index - (crossAxisCount - yu);
            item = img[realIndex];
          }
        } else {
          item = index < img.length ? img[index] : null;
        }
        if (item == null) {
          list1.add(const Spacer());
        } else {
          list1.add(Expanded(
            child: GestureDetector(
              onTap: () {
                if (isThumbnail) {
                  var bigImgList = imgList?.map((e) {
                    if (e.indexOf('?') == -1) return e;
                    return e.substring(0, e.indexOf('?'));
                  }).toList();
                  UiUtils.showImage(context, bigImgList, realIndex);
                } else {
                  UiUtils.showImage(context, imgList, realIndex);
                }
              },
              behavior: HitTestBehavior.translucent,
              child: AspectRatio(
                aspectRatio: 1,
                child: UiUtils.getNetImageFit(
                  item,
                  fit: BoxFit.cover,
                  cacheWidthRate: .3,
                ),
              ),
            ),
          ));
        }
      }
      list.add(Row(
        children: list1,
      ));
    }
    return Column(
      children: list,
    );
  }
}
