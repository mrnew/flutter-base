import 'package:flutter/material.dart';

class ImagesAnim extends StatefulWidget {
  final List<String> imageCaches;

  const ImagesAnim(this.imageCaches, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _WOActionImageState();
  }
}

class _WOActionImageState extends State<ImagesAnim> {
  late bool _disposed;
  late Duration _duration;
  late int _imageIndex;
  late Container _container;

  @override
  void initState() {
    super.initState();
    _disposed = false;
    _duration = const Duration(milliseconds: 200);
    _imageIndex = 1;
    _container = Container();
    _updateImage();
  }

  void _updateImage() {
    if (_disposed || widget.imageCaches.isEmpty) {
      return;
    }

    setState(() {
      if (_imageIndex > widget.imageCaches.length) {
        _imageIndex = 1;
      }
      _container = Image.asset(
        widget.imageCaches[_imageIndex],
        gaplessPlayback: true,
      ) as Container;
      _imageIndex++;
      if (_imageIndex >= widget.imageCaches.length) {
        _imageIndex--;
      }
    });
    Future.delayed(_duration, () {
      _updateImage();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _disposed = true;
  }

  @override
  Widget build(BuildContext context) {
    return _container;
  }
}
