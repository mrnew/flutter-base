import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/event_bus.dart';

import 'state.dart';

class SecondLogic extends BaseLogic {
  final SecondState data = SecondState();

  @override
  void onReady() {
    super.onReady();
    data.subscriptions.add(LibUtils.eventBus.on<InitUserEvent>().listen((event) {
     update();
    }));
    data.subscriptions.add(LibUtils.eventBus.on<LoginEvent>().listen((event) {
      update();
    }));
    data.subscriptions.add(LibUtils.eventBus.on<LogoutEvent>().listen((event) {
      update();
    }));
    update();
  }

}
