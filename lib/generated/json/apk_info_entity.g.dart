import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/apk_info_entity.dart';

ApkInfoEntity $ApkInfoEntityFromJson(Map<String, dynamic> json) {
	final ApkInfoEntity apkInfoEntity = ApkInfoEntity();
	final String? content = jsonConvert.convert<String>(json['content']);
	if (content != null) {
		apkInfoEntity.content = content;
	}
	final String? forceUpgrade = jsonConvert.convert<String>(json['forceUpgrade']);
	if (forceUpgrade != null) {
		apkInfoEntity.forceUpgrade = forceUpgrade;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		apkInfoEntity.url = url;
	}
	final String? v = jsonConvert.convert<String>(json['v']);
	if (v != null) {
		apkInfoEntity.v = v;
	}
	return apkInfoEntity;
}

Map<String, dynamic> $ApkInfoEntityToJson(ApkInfoEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['content'] = entity.content;
	data['forceUpgrade'] = entity.forceUpgrade;
	data['url'] = entity.url;
	data['v'] = entity.v;
	return data;
}