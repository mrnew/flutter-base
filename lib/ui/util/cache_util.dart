import 'dart:io';

import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

///加载缓存
Future<String> loadCache() async {
  try {
    var directory = await getTemporaryDirectory();
    var path = p.join(directory.path, DefaultCacheManager.key);
    Directory tempDir = Directory(path);
    double value = await _getTotalSizeOfFilesInDir(tempDir);
    print('临时目录大小: ' + value.toString());
    var str = _renderSize(value);
    return str;
  } catch (err) {
    print(err);
  }
  return '';
}

/// 递归方式 计算文件的大小
Future<double> _getTotalSizeOfFilesInDir(final FileSystemEntity file) async {
  try {
    if (file is File) {
      int length = await file.length();
      return double.parse(length.toString());
    }
    if (file is Directory) {
      final List<FileSystemEntity> children = file.listSync();
      double total = 0;
      for (final FileSystemEntity child in children)
        total += await _getTotalSizeOfFilesInDir(child);
      return total;
    }
    return 0;
  } catch (e) {
    print(e);
    return 0;
  }
}

///格式化文件大小
_renderSize(double value) {
  List<String> unitArr = []..add('B')..add('K')..add('M')..add('G');
  int index = 0;
  while (value > 1024) {
    index++;
    value = value / 1024;
  }
  String size = value.toStringAsFixed(2);
  return size + unitArr[index];
}

Future<bool> clearCache() async {
  //此处展示加载loading
  try {
    var directory = await getTemporaryDirectory();
    var path = p.join(directory.path, DefaultCacheManager.key);
    Directory tempDir = Directory(path);
    //删除缓存目录
    await delDir(tempDir);
    LibUtils.showToast('清除缓存成功');
    return true;
  } catch (e) {
    print(e);
    LibUtils.showToast('清除缓存失败');
    return false;
  }
}

///递归方式删除目录
Future<Null> delDir(FileSystemEntity file) async {
  try {
    if (file is Directory) {
      final List<FileSystemEntity> children = file.listSync();
      for (final FileSystemEntity child in children) {
        await delDir(child);
      }
    }
    await file.delete();
  } catch (e) {
    print(e);
  }
}
