import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wameiji/ui/base/page.dart';

class LoginSysState with BaseStateMixin {
  TextEditingController unameController = TextEditingController();
  TextEditingController pwdController = TextEditingController();
  RxBool isArgree = false.obs;
  RxBool hasVisibility = false.obs;
}
