import 'package:get/get.dart';
import 'package:mrnew_libary/util/check_util.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'state.dart';

class ModifyPwdLogic extends BaseLogic {
  final ModifyPwdState data = ModifyPwdState();

  void onSubmit() {
    LibUtils.hideKeyboard(Get.context!);

    var name = data.oldController.text;
    var pwd = data.pwdController.text;
    var pwdSure = data.pwdSureController.text;
    if (UiUtils.isEmpty(name)) {
      LibUtils.showToast("请输入原始密码");
      return;
    }
    if (pwd.length < 6 || pwd.length > 20) {
      LibUtils.showToast("请输入6~20位的新密码");
      return;
    }
    if (!CheckUtil.checkSpecialCharacter(pwd)) {
      LibUtils.showToast("密码为字母、数字");
      return;
    }
    if (pwd != pwdSure) {
      LibUtils.showToast("再次输入的密码不一致");
      return;
    }
    HttpClient.instance.request(
      {
        'n': 'Sig.Front.User.AppUserSetting.ProfileEditPassword',
        'oldPassword': name,
        'newPassword': pwd,
        'reNewPassword': pwdSure,
      },
      isGet: false,
      cancelToken: cancelToken,
      isShowLoading: true,
      successText: '修改密码成功，请重新登录',
      success: (Object? entity) {
        UiUtils.logout(Get.context);
      },
    );
  }
}
