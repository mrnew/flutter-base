import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:dio/src/cancel_token.dart' as dioCancel;
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/model/test_list_entity.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:worker_manager/worker_manager.dart';

enum Method { get, post }

typedef Success<T> = void Function(T? entity);
typedef Error = void Function(HttpException exception);
typedef HandleData = void Function(Map<String, dynamic>? ret);
typedef Result = void Function(Map<String, dynamic>? ret);

class HttpException implements Exception {
  final String? msg;
  final String code;

  ///是否本地错误
  final bool isLocal;
  final dynamic data;

  const HttpException(this.code, this.msg, {this.isLocal = false, this.data});
}

class HttpClient {
  ///测试平台
  static String platformUrl0 = 'http://150.158.77.232:8888/';

  ///生产平台
  static String platformUrl1 = 'http://150.158.77.232:8888/';

  ///是否测试平台
  static bool isTest = false;
  static String baseUrl = platformUrl1;

  factory HttpClient() => _getInstance();

  static HttpClient get instance => _getInstance();
  static HttpClient? _instance;

  DioMixin? dio;

  static HttpClient _getInstance() {
    if (_instance == null) {
      _instance = HttpClient._internal();
    }
    return _instance!;
  }

  HttpClient._internal();

  Future<void> restart() async {
    await init();
  }

  Future<void> init() async {
    // 初始化
    if (GlobalConfig.isTest) {
      var str = await CacheManager.getInstance().get(false, 'isTest', 'false');
      var test = str == 'true';
      isTest = test;
    } else {
      isTest = false;
    }
    if (isTest) {
      baseUrl = platformUrl0;
    } else {
      var mercari =
          await CacheManager.getInstance().get(false, 'URL_CONFIG', null);
      if (UiUtils.isEmpty(mercari)) {
        baseUrl = platformUrl1;
      } else {
        baseUrl = mercari!;
      }
    }
    //重新初始化
    dio = DioForNative(BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: GlobalConfig.isTest
          ? const Duration(seconds: 30)
          : const Duration(seconds: 10),
      receiveTimeout: const Duration(seconds: 30),
    ));
    try {
      if (GlobalConfig.isOpenHttpLog) {
        dio!.interceptors.add(PrettyDioLogger(
            requestHeader: true,
            requestBody: true,
            responseHeader: true,
            maxWidth: 600));
      }
    } catch (e) {}
  }

  request<T>(Map<String, dynamic> param,
      {String url = '',
      bool isGet = true,
      Success<T>? success,
      Map<String, dynamic>? urlParams,
      String? cacheKey,
      bool isRetry = true,
      bool isShowLoading = false,

      ///成功是否隐藏loading
      bool isSuccessHideLoading = true,

      ///是否显示错误信息
      bool isShowErrorMsg = true,

      ///成功后的toast
      String? successText,
      dioCancel.CancelToken? cancelToken,
      Result? result,
      Error? error,
      HandleData? handleData}) {
    if (isShowLoading) {
      LibUtils.showLoading();
    }
    Success<T> realSuccess = (entity) {
      if (isShowLoading && isSuccessHideLoading) {
        LibUtils.hideLoading();
      }
      if (LibUtils.isNotEmpty(successText)) {
        LibUtils.showToast(successText);
      }
      success?.call(entity);
    };
    Error realError = (exception) {
      if (isShowLoading) {
        LibUtils.hideLoading();
      }
      if (isShowErrorMsg) {
        LibUtils.showToast(exception.msg);
      }
      error?.call(exception);
    };
    try {
      var params = <String, dynamic>{};
      params.addAll(param);

      if (isGet) {
        params['from'] = Platform.isAndroid ? 'Android' : 'IOS';
        params['v'] = GlobalConfig.version ?? '';
      } else {
        if (url.contains('?')) {
          url += '&v=' + GlobalConfig.version!;
        } else {
          url += '?v=' + GlobalConfig.version!;
        }
        url += '&from=' + (Platform.isAndroid ? 'Android' : 'IOS');
        if (params.containsKey('n')) {
          url += '&n=' + params['n'];
          params.remove('n');
        }
        if (urlParams == null) {
          urlParams = {};
        }
        urlParams.forEach((key, value) {
          url += '&' + key + '=' + value.toString();
        });
      }
      if (T.toString() == 'TestListEntity') {
        dynamic data = TestListEntity()
          ..list = [
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
            TestListListEntity(),
          ];
        success?.call(data);
        return;
      }
      _doRequest<T>(url, params, isGet ? Method.get : Method.post,
          cacheKey: cacheKey,
          isRetry: isRetry,
          cancelToken: cancelToken,
          success: realSuccess,
          result: result,
          error: realError,
          handleData: handleData);
    } catch (e, stack) {
      print(e);
      print(stack);
      if (error != null) {
        realError(const HttpException("-1", "网络无法连接，请稍后重试"));
      }
    }
  }

  void _doRequest<T>(String url, Map<String, dynamic> params, Method method,
      {String? cacheKey,
      bool isRetry = true,
      int? newBakIndex,
      Success<T>? success,
      dioCancel.CancelToken? cancelToken,
      Result? result,
      Error? error,
      HandleData? handleData}) async {
    if (dio == null) {
      await init();
    }
    Response<String>? response;
    try {
      switch (method) {
        case Method.get:
          response = await dio!.get<String>(url,
              queryParameters: params, cancelToken: cancelToken);
          break;
        case Method.post:
          response = await dio!
              .post<String>(url, data: params, cancelToken: cancelToken);
          break;
      }
    } catch (exception) {
      print('错误：${url},${exception.toString()}');
      if (exception is DioError) {
        if (exception.type == DioErrorType.cancel) {
          print('取消：${url}');
          return;
        }
        if (exception.response?.statusCode == 401) {
          _logout();
          return;
        }
      }
      if (!isRetry) {
        if (error != null) {
          error(const HttpException("-1", "网络无法连接，请稍后重试", isLocal: true));
        }
        return;
      }
      _doRequest<T>(url, params, method,
          cacheKey: cacheKey,
          isRetry: false,
          cancelToken: cancelToken,
          success: success,
          result: result,
          error: error,
          handleData: handleData);
      return;
    }
    _onResponse<T>(response,
        cacheKey: cacheKey,
        success: success,
        result: result,
        error: error,
        handleData: handleData);
  }

  _onResponse<T>(Response<String>? response,
      {String? cacheKey,
      Success<T>? success,
      Result? result,
      Error? error,
      HandleData? handleData}) async {
    try {
      if (response?.statusCode == 401) {
        _logout();
        return;
      }
      if (response == null || response.statusCode! >= 400) {
        if (error != null) {
          error(const HttpException("-1", "网络无法连接，请稍后重试"));
        }
        return;
      }

      //线程池
      Map para = {'data': response.data, 'type': T.toString()};
      Map retMap = await workerManager.execute(() => parseJson2Map(para));
      if (retMap.containsKey('error')) {
        throw retMap['error'];
      }
      Map<String, dynamic> ret = retMap['ret'];
      var data = retMap['data'];

      if (ret['code'] != 200) {
        if (ret['code'] == 403) {
          _logout();
          return;
        }
        if (error != null) {
          error(HttpException(
              ret['code']?.toString() ?? "-1", ret['msg'].toString(),
              data: ret['data']));
        }
        return;
      }

      if (result != null) {
        result(ret);
      }
      if (success != null) {
        try {
          success(data);
        } catch (e1, stack1) {
          print(e1);
          print(stack1);
        }
      }
      if (cacheKey != null) {
        CacheManager.getInstance().put(false, cacheKey, response.data);
      }
    } catch (e, stack) {
      print(e);
      print(stack);
      if (error != null) {
        error(const HttpException("-1", "服务器数据有误，请稍后重试"));
      }
    }
  }

  _logout() {
    LibUtils.hideLoading();
    UiUtils.logout(null);
  }

  Future<String?> upload(String url, String path, String name,
      {dioCancel.CancelToken? cancelToken}) async {
    FormData formdata = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        path, //图片路径
        filename: name,
      )
    });
    var response;
    try {
      response = await dio!.post(
        //上传结果
        baseUrl + url,
        options:
            Options(headers: {'Authorization': UserManager.user.value?.token}),
        data: formdata, cancelToken: cancelToken,
      );
    } catch (exception) {
      print(exception);
      throw const HttpException("-1", "网络无法连接，请稍后重试");
    }
    Map<String, dynamic>? ret;
    try {
      ret = json.decode(response.toString());
    } catch (e, s) {
      print(e);
      print(s);
      throw const HttpException("-1", "服务器数据有误，请稍后重试");
    }
    if (ret!['code'] != 200) {
      throw HttpException("-1", ret['message'].toString());
    }
    try {
      if (ret['data'] is List) {
        return ret['data'][0].toString();
      }
      throw const HttpException("-1", "服务器数据有误，请稍后重试");
    } catch (e, s) {
      print(e);
      print(s);
      throw const HttpException("-1", "服务器数据有误，请稍后重试");
    }
  }
}

///解析json 顶级函数
Map parseJson2Map(Map map) {
  var ret = json.decode(map['data']);
  var type = map['type'];
  var data;
  try {
    if (ret['code'] == 200 && ret['data'] != null) {
      if (type.startsWith('Map') ||
          type == 'Object' ||
          type == 'double' ||
          type == 'int' ||
          type == 'bool') {
        data = ret['data'];
      } else if (type.startsWith('String')) {
        data = ret['data']?.toString();
      } else if (type.startsWith('List<String>')) {
        data = (ret['data'] as List).cast<String>();
      } else {
        data = JsonConvert.fromJsonAsT(ret['data'], type);
      }
    }
  } catch (e, stack) {
    print(e);
    print(stack);
    return {'error': e.toString()};
  }
  return {'ret': ret, 'data': data};
}
