import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/check_util.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/user_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/event_bus.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'state.dart';

class RegisterLogic extends BaseLogic {
  final RegisterState data = RegisterState();

  void onCode() {
    var mobile = data.phoneController.text;
    if (UiUtils.isEmpty(mobile)) {
      LibUtils.showToast("请输入手机号");
      Future.delayed(Duration.zero, () {
        if (data.ctr != null && !isClosed) {
          data.ctr!.reset();
        }
      });
      return;
    }

    HttpClient.instance.request<Object>({
      'n': 'Sig.Front.AppApi.SendVerifyCodeAction',
      'type': 'SMS',
      'user': mobile,
    }, isGet: false, cancelToken: cancelToken, successText: "短信验证码已发送",
        error: (HttpException e) {
      if (data.ctr != null && !isClosed) {
        data.ctr!.reset();
      }
    });
  }

  void onSubmit() {
    LibUtils.hideKeyboard(Get.context!);
    var param;

    var name = data.phoneController.text;
    var code = data.codeController.text;
    var pwd = data.pwdController.text;
    var pwdSure = data.pwdSureController.text;
    if (UiUtils.isEmpty(name)) {
      LibUtils.showToast("请输入手机号");
      return;
    }
    if (!CheckUtil.isPhone(name)) {
      LibUtils.showToast("请正确填写手机号码");
      return;
    }
    if (UiUtils.isEmpty(code)) {
      LibUtils.showToast("请输入验证码");
      return;
    }
    if (pwd.length < 6 || pwd.length > 20) {
      LibUtils.showToast("请输入6~20位的新密码");
      return;
    }
    if (!CheckUtil.checkSpecialCharacter(pwd)) {
      LibUtils.showToast("密码为字母、数字");
      return;
    }
    if (pwd != pwdSure) {
      LibUtils.showToast("再次输入的密码不一致");
      return;
    }
    param = {
      'n': 'Sig.Front.AppApi.ResetPasswordAction',
      'type': 'SMS',
      'user': name,
      'code': code,
      'password': pwd,
    };
    HttpClient.instance.request(
      param,
      isGet: false,
      cancelToken: cancelToken,
      successText: '注册成功',
      isShowLoading: true,
      success: (Object? entity) {
        getInfo(data.phoneController.text, Get.context!);
      },
    );
  }

  void getInfo(String name, BuildContext context) {
    HttpClient.instance.request<UserEntity>(
      {
        'n': 'Sig.Front.User.AppUserSetting.GetUserInfo',
      },
      isGet: false,
      cancelToken: cancelToken,
      isShowLoading: false,
      success: (UserEntity? data) {
        data!.localId = name;
        CacheManager.getInstance().put(false, 'lastName', name);
        UserManager.save(data);
        LibUtils.eventBus.fire(const LoginEvent());
        LibUtils.hideLoading();
        Get.until(ModalRoute.withName('main_page'));
      },
    );
  }
}
