import 'package:mrnew_libary/widget/js/js_bridge.dart';
import 'package:mrnew_libary/widget/web_progress.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebState with BaseStateMixin {
  String? url;
  String? title;
  JsBridge jsBridge = JsBridge();
  WebProgressController progressController = WebProgressController();
  WebViewController webController = WebViewController();
}
