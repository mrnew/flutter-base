import 'package:package_info/package_info.dart';

class GlobalConfig {
  ///控制推送平台、切换平台
  static bool isTest = true;

  ///控制请求日志
  static bool isOpenHttpLog = true;

  ///是否检测内存泄漏
  static bool isCheckMemory = false;

  static bool isTable = false;

  ///是否有开屏广告
  static bool hasAdSplash = false;

  ///是否有启动页
  static bool hasSplash = false;

  ///是否必须登录
  static bool isMustLogin = false;

  ///是否有首次登录隐私政策弹窗
  static bool hasFirstUserNoticeDialog = true;

  ///是否已初始化好用户
  static bool isInitUser = false;

  ///用户协议
  static String userProtocolUrl = '/?n=Sig.Front.User.AppUser.UserProtocolMC';

  ///隐私政策
  static String userPrivacyUrl = '/?n=Sig.Front.User.AppUser.PrivacyPolicyMC';
  static String? versionConfig;
  static String? version;

  static String historyKey = 'HistoryKey';

  static void init() async {
    var info = await PackageInfo.fromPlatform();
    version = info.version;

    ///是否适配pad
    // isTable = min(Get.width, Get.height) >= 600;
  }
}
