import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/base/page.dart';

import 'logic.dart';
import 'state.dart';

class AboutPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<AboutPage, AboutLogic, AboutState> {
  AboutLogic createLogic() {
    return AboutLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(context, logic, () {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const Spacer(
            flex: 22,
          ),
          Center(
              child: GestureDetector(
                  onLongPress: () {
                    logic.onChange();
                  },
                  child: const Image(image: AssetImage(Res.about_logo)))),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Obx(() => Text(
                  '版本号 V' + state.version.value,
                  style:
                      const TextStyle(color: Color(0xff9B9B9B), fontSize: 12.0),
                )),
          ),
          const Spacer(
            flex: 36,
          ),
          const SafeArea(
            child: Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Text(
                'Copyright © 2020-2022 挖煤姬 meruki.cn 版权所有',
                style: TextStyle(
                  fontSize: 11,
                  color: Color(0xff9B9B9B),
                ),
              ),
            ),
          )
        ],
      );
    }, title: '关于我们');
  }
}
