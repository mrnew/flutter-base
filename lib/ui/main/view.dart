import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/app.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/common/event_bus.dart';
import 'package:wameiji/ui/main/index/view.dart';
import 'package:wameiji/ui/main/mine/view.dart';
import 'package:wameiji/ui/main/second/view.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'logic.dart';
import 'state.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends BasePageState<MainPage, MainLogic, MainState>
    with RouteAware, WidgetsBindingObserver {
  @override
  MainLogic createLogic() {
    return MainLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
            backgroundColor: MyColor.globalBg,
            bottomNavigationBar: Theme(
              data: ThemeData(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
              ),
              child: Obx(() {
                return Stack(children: <Widget>[
                  BottomNavigationBar(
                      backgroundColor: Colors.white,
                      selectedItemColor: const Color(0xffd55b74),
                      unselectedItemColor: const Color(0xff5e616b),
                      selectedLabelStyle: const TextStyle(
                          fontSize: 10, fontWeight: FontWeight.w500),
                      unselectedLabelStyle: const TextStyle(
                          fontSize: 10, fontWeight: FontWeight.w500),
                      items: const [
                        BottomNavigationBarItem(
                          icon: Image(
                              image: AssetImage(Res.main_tab0),
                              height: 26,
                              gaplessPlayback: true),
                          label: "首页",
                          activeIcon: Image(
                              image: AssetImage(Res.main_tab0_),
                              height: 26,
                              gaplessPlayback: true),
                        ),
                        BottomNavigationBarItem(
                          icon: Image(
                              image: AssetImage(Res.main_tab1),
                              height: 26,
                              gaplessPlayback: true),
                          label: "分类",
                          activeIcon: Image(
                              image: AssetImage(Res.main_tab1_),
                              height: 26,
                              gaplessPlayback: true),
                        ),
                        BottomNavigationBarItem(
                          icon: Image(
                              image: AssetImage(Res.main_tab2),
                              height: 26,
                              gaplessPlayback: true),
                          label: "购物车",
                          activeIcon: Image(
                              image: AssetImage(Res.main_tab2_),
                              height: 26,
                              gaplessPlayback: true),
                        ),
                        BottomNavigationBarItem(
                          icon: Image(
                              image: AssetImage(Res.main_tab3),
                              height: 26,
                              gaplessPlayback: true),
                          label: "我的",
                          activeIcon: Image(
                              image: AssetImage(Res.main_tab3_),
                              height: 26,
                              gaplessPlayback: true),
                        )
                      ],
                      type: BottomNavigationBarType.fixed,
                      //默认选中首页
                      currentIndex: state.tabIndex.value,
                      onTap: (index) => state.tabIndex.value = index),
                ]);
              }),
            ),
            body: Obx(() {
              return IndexedStack(
                index: state.tabIndex.value,
                children: <Widget>[
                  IndexPage(),
                  SecondPage(),
                  SecondPage(),
                  MinePage(),
                ],
              );
            })),
      ],
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute<dynamic>);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    LibUtils.eventBus.fire(AppLifecycleChange(state));
  }

  @override
  void didPushNext() {
    LibUtils.eventBus.fire(const MainPageUnvisableEvent());
  }

  @override
  void didPopNext() {
    LibUtils.eventBus.fire(const MainPageVisableEvent());
  }
}
