import 'package:wameiji/generated/json/base/json_field.dart';
import 'package:wameiji/generated/json/apk_info_entity.g.dart';


@JsonSerializable()
class ApkInfoEntity {

	ApkInfoEntity();

	factory ApkInfoEntity.fromJson(Map<String, dynamic> json) => $ApkInfoEntityFromJson(json);

	Map<String, dynamic> toJson() => $ApkInfoEntityToJson(this);

	String? content;
	String? forceUpgrade;
	String? url;
	String? v;
}
