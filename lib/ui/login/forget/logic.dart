import 'package:get/get.dart';
import 'package:mrnew_libary/util/check_util.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'state.dart';

class ForgetLogic extends BaseLogic {
  final ForgetState data = ForgetState();

  @override
  void onClose() {
    super.onClose();
    data.ctr?.dispose();
  }

  void onCode() {
    var mobile = data.phoneController.text;
    if (UiUtils.isEmpty(mobile)) {
      LibUtils.showToast("请输入手机号");
      Future.delayed(Duration.zero, () {
        if (data.ctr != null && !isClosed) {
          data.ctr!.reset();
        }
      });
      return;
    }

    HttpClient.instance.request<Object>({
      'n': 'Sig.Front.AppApi.SendVerifyCodeAction',
      'type': 'SMS',
      'user': mobile,
    }, isGet: false, cancelToken: cancelToken, successText: "短信验证码已发送",
        error: (HttpException e) {
      if (data.ctr != null && !isClosed) {
        data.ctr!.reset();
      }
    });
  }

  void onSubmit() {
    LibUtils.hideKeyboard(Get.context!);
    var param;

    var name = data.phoneController.text;
    var code = data.codeController.text;
    var pwd = data.pwdController.text;
    var pwdSure = data.pwdSureController.text;
    if (UiUtils.isEmpty(name)) {
      LibUtils.showToast("请输入手机号");
      return;
    }
    if (!CheckUtil.isPhone(name)) {
      LibUtils.showToast("请正确填写手机号码");
      return;
    }
    if (UiUtils.isEmpty(code)) {
      LibUtils.showToast("请输入验证码");
      return;
    }
    if (pwd.length < 6 || pwd.length > 20) {
      LibUtils.showToast("请输入6~20位的新密码");
      return;
    }
    if (!CheckUtil.checkSpecialCharacter(pwd)) {
      LibUtils.showToast("密码为字母、数字");
      return;
    }
    if (pwd != pwdSure) {
      LibUtils.showToast("再次输入的密码不一致");
      return;
    }
    param = {
      'n': 'Sig.Front.AppApi.ResetPasswordAction',
      'type': 'SMS',
      'user': name,
      'code': code,
      'password': pwd,
    };
    HttpClient.instance.request(
      param,
      cancelToken: cancelToken,
      successText: '修改密码成功',
      isShowLoading: true,
      success: (Object? data) {
        Get.back();
      },
      isGet: false,
    );
  }
}
