import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:mrnew_libary/widget/flutter_countdown.dart';
import 'package:mrnew_libary/widget/icon_text.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/util/page_utils.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:wameiji/ui/widget/business/global_action_btn.dart';

import 'logic.dart';
import 'state.dart';

class LoginCodePage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State
    extends BasePageState<LoginCodePage, LoginCodeLogic, LoginCodeState> {
  @override
  LoginCodeLogic createLogic() {
    return LoginCodeLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(
      context,
      logic,
      () {
        return _getContent(logic, state, context);
      },
      hasAppBarLeading: false,
      title: '验证码登录',
      elevation: 0,
    );
  }
}

_getContent(LoginCodeLogic logic, LoginCodeState state, BuildContext context) {
  return ListView(
    children: <Widget>[
      Container(
        margin: const EdgeInsets.fromLTRB(15, 30, 15, 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            // 手机号验证码登录
            const Padding(
              padding: EdgeInsets.only(bottom: 30),
              child: Text(
                "手机号验证码登录",
                style: TextStyle(
                    color: Color(0xff333333),
                    fontWeight: FontWeight.w500,
                    fontSize: 24.0),
              ),
            ), // 矩形备份 7
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  color: Color(0xffffffff)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TextField(
                    controller: state.unameController,
                    style: const TextStyle(
                        color: Color(0xff333333), fontSize: 14.0),
                    decoration: const InputDecoration(
                      hintStyle: TextStyle(
                          color: Color(0xff999999), fontSize: 14.0),
                      hintText: "请输入手机号码",
                      contentPadding: EdgeInsets.symmetric(vertical: 15),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                    ),
                    inputFormatters: [LengthLimitingTextInputFormatter(11)],
                    keyboardType: TextInputType.phone,
                  ),
                  MyStyle.divider,
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: state.codeController,
                          style: const TextStyle(
                              color: Color(0xff333333), fontSize: 14.0),
                          decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                color: Color(0xff999999), fontSize: 14.0),
                            hintText: "请输入验证码",
                            contentPadding: EdgeInsets.symmetric(vertical: 15),
                            border:
                                OutlineInputBorder(borderSide: BorderSide.none),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        behavior: HitTestBehavior.translucent,
                        child: CountDown(
                          renderSemanticLabel: (count) {
                            if (count == 60) {
                              return "发送验证码";
                            }
                            return "$count 秒后重试";
                          },
                          beginCount: 60,
                          endCount: 0,
                          textStyle: const TextStyle(
                              color: Color(0xff333333), fontSize: 14.0),
                          refs: (ctr) {
                            state.ctr = ctr;
                          },
                          statusListener: (status) {},
                          onPress: (ctr) {
                            if (ctr.isAnimating) {
                              return Future.value(false);
                            }
                            logic.onSms();
                            return Future.value(true);
                          },
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                // 注册新用户
                GestureDetector(
                  onTap: () {
                    Get.toNamed('register_page',
                        arguments: true, preventDuplicates: false);
                  },
                  behavior: HitTestBehavior.translucent,
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    child: Text("注册新用户",
                        style: TextStyle(
                            color: Color(0xff0088ff), fontSize: 14.0)),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: GlobalActionBtn(
                title: '登录',
                onPressed: () {
                  logic.onLogin(context);
                },
              ),
            ),
            Center(
              child: GestureDetector(
                onTap: () {
                  Get.offNamed('login_page');
                },
                behavior: HitTestBehavior.translucent,
                child: const Padding(
                  padding: EdgeInsets.only(top: 20.0, bottom: 10),
                  child: IconText(
                    "密码登录",
                    style: TextStyle(color: Color(0xff333333), fontSize: 14.0),
                    isReverse: true,
                    padding: 5,
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      size: 12,
                      color: Color(0xff333333),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(() {
                    return SizedBox(
                      width: 35,
                      height: 20,
                      child: Checkbox(
                        value: state.isArgree.value,
                        focusColor: const Color(0xff0088ff),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                        ),
                        onChanged: (value) =>
                            state.isArgree.value = value ?? false,
                      ),
                    );
                  }),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: '我已阅读并同意',
                            style: MyText.textStyle14_333,
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                state.isArgree.value = true;
                              }),
                        TextSpan(
                            text: ' 《售后服务隐私协议》 ',
                            style: MyText.textStyle14_0088ff,
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                PageUtils.startWeb(
                                    GlobalConfig.userPrivacyUrl, '售后服务隐私协议');
                              })
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ],
  );
}
