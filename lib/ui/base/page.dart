// ignore_for_file: invalid_use_of_protected_member

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:uuid/uuid.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:wameiji/ui/widget/loading.dart';

import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

mixin BaseStateMixin {
  //页面tag标记
  String pageTag = const Uuid().v4();
  RefreshController controller = RefreshController(initialRefresh: false);

  //监听
  List<StreamSubscription> subscriptions = [];
  int page = 1;
  bool isList = false;

  //是否单页面
  bool isSinglePage = false;

  //是否使用GetBuilder
  bool isGetBuilder = false;
}

typedef DataBind = void Function(BaseStateMixin newState, dynamic data);
typedef GetPageContent = Widget Function();

abstract class BaseLogic<T> extends GetxController {
  bool isReadyLoadData = true;

  BaseStateMixin get data => data;
  StateMixin? _stateMixin;

  ///取消网络请求
  CancelToken? _cancelToken;

  CancelToken? get cancelToken {
    if (_cancelToken == null) {
      _cancelToken = CancelToken();
      return _cancelToken;
    }
    return _cancelToken;
  }

  //获取数据、
  void getData() {}

  //有刷新动画的刷新
  void onAutoRefresh() {
    try {
      data.controller.requestRefresh();
    } catch (e) {
      e.printError();
    }
  }

  //刷新
  void onRefresh() {
    if (isClosed) return;
    data.page = 1;
    getData();
  }

  //获取更多
  void loadMore() {
    if (isClosed) return;
    data.page++;
    getData();
  }

  @override
  void onReady() {
    super.onReady();
    if (this is StateMixin) {
      _stateMixin = this as StateMixin;
    }
    if (isReadyLoadData) onRefresh();
  }

  ///是否有更多数据
  bool isNoMore(T? entity) {
    return true;
  }

  ///是否显示空界面
  bool isEmpty(T? entity) {
    return false;
  }

  T? get info => _stateMixin?.state;

  ///刷新getChild所有节点
  void refreshBody() {
    if (_stateMixin?.status.isSuccess ?? false) {
      _stateMixin?.change(info, status: RxStatus.success());
    }
  }

  ///清除数据
  void clearAndEmpty() {
    data.page == 1;
    _stateMixin?.change(null, status: RxStatus.empty());
  }

  ///清除并请求数据
  void clearAndRefresh() {
    data.controller.refreshToIdle();
    data.controller.loadComplete();
    _stateMixin?.change(null, status: RxStatus.loading());
    onRefresh();
  }

  ///处理数据
  T? handleData(T? oldData, T? newData, bool isAdd) {
    return null;
  }

  void autoLoadPad(T? list, {int maxPage = 2}) {
    if (GlobalConfig.isTable) {
      if (data.page < maxPage && !isNoMore(list)) {
        loadMore();
      }
    }
  }

  //页面成功
  void onPageSuccess(T? entity) {
    var noMore = data.isList ? isNoMore(entity) : false;
    var result = data.isList
        ? handleData(_stateMixin?.state, entity, data.page != 1)
        : entity;

    data.controller.refreshCompleted();
    if (noMore == true) {
      data.controller.loadNoData();
    } else {
      data.controller.loadComplete();
    }

    _stateMixin?.change(result,
        status: (data.isList ? isEmpty(result) : result == null)
            ? RxStatus.empty()
            : RxStatus.success());
  }

  //页面失败
  void onPageFaild(HttpException e) {
    if (data.controller.isRefresh) {
      data.controller.refreshFailed();
    }
    if (data.controller.isLoading) {
      data.controller.loadFailed();
    }
    if (_stateMixin != null && _stateMixin?.status.isSuccess == false) {
      //不在成功页才展示错误页
      _stateMixin?.change(info, status: RxStatus.error(e.msg));
    }
  }

  @override
  void onClose() {
    cancelToken?.cancel('page close');
    _cancelToken = null;
    super.onClose();
    data.controller.dispose();
    data.subscriptions.forEach((element) {
      element.cancel();
    });
    data.subscriptions.clear();
  }

  void showLoading({String? text}) {
    if (text != null) {
      LibUtils.showLoading(text: text);
    } else {
      LibUtils.showLoading();
    }
  }

  void hideLoading() {
    LibUtils.hideLoading();
  }

  void showToast(String? string) {
    LibUtils.showToast(string);
  }
}

///通用页面结构
Widget myPage(
  BuildContext context,
  BaseLogic? logic,
  GetPageContent getChild, {
  String? title,
  Widget? titleWidget,
  TextStyle titleStyle = MyText.textStyle18_333,
  BaseStateMixin? state,
  bool enablePullUp = false, //是否显示加载更多
  bool isRefersh = false, //是否显示刷新
  List<Widget>? bannerRightAction,
  double elevation = 1,
  PreferredSizeWidget? appBarBottom,
  Widget? endDrawer,
  Widget? startDrawer,
  Widget? bottomNavigationBar,
  Widget? floatingActionButton,
  Color? bottomNavigationBarBg,
  Color appBarColor = Colors.white,
  bool hasAppBarLeading = true,
  ScrollController? scrollController,
  double titleSpacing = NavigationToolbar.kMiddleSpacing,
  GlobalKey<ScaffoldState>? key,
  bool endDrawerEnableOpenDragGesture = true,
  bool drawerEnableOpenDragGesture = true,
  bool extendBodyBehindAppBar = false,
  Color bg = MyColor.globalBg,
  bool? resizeToAvoidBottomInset,
  Widget? leading,
  SystemUiOverlayStyle appbarBrightness = SystemUiOverlayStyle.dark,
  Widget errorWidget = const GlobalError(),
  Widget emptyWidget = const GlobalEmty(),
  Widget loadingWidget = const GlobalLoading(),
  bool isErrorRetry = true,
  bool isIngoreKeyboardDismisser = false,
}) {
  var body;
  if (logic != null && logic is StateMixin) {
    //有状态
    body = (logic as StateMixin).obx(
      (state1) {
        if (isRefersh && state != null) {
          return SmartRefresher(
            controller: state.controller,
            scrollController: scrollController,
            enablePullUp: enablePullUp,
            onRefresh: () {
              //刷新
              logic.onRefresh();
            },
            onLoading: () {
              logic.loadMore();
            },
            child: getChild(),
          );
        } else {
          return getChild();
        }
      },
      onLoading: loadingWidget,
      onEmpty: isRefersh && state != null
          ? SmartRefresher(
              controller: state.controller,
              scrollController: scrollController,
              enablePullUp: enablePullUp,
              onRefresh: () {
                logic.onRefresh();
              },
              child: emptyWidget,
            )
          : emptyWidget,
      onError: (error) => !isErrorRetry
          ? errorWidget
          : GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                //重试
                (logic as StateMixin).change(null, status: RxStatus.loading());
                logic.onRefresh();
              },
              child: errorWidget,
            ),
    );
  } else {
    if (isRefersh && state != null) {
      body = SmartRefresher(
        controller: state.controller,
        scrollController: scrollController,
        enablePullUp: enablePullUp,
        onRefresh: () async {
          //刷新
          logic?.onRefresh();
        },
        onLoading: () {
          logic?.loadMore();
        },
        child: getChild(),
      );
    } else {
      body = getChild();
    }
  }

  if (title != null) {
    titleWidget = Text(
      title,
      style: titleStyle,
    );
  }
  var appbar;
  if (titleWidget != null) {
    appbar = AppBar(
      title: titleWidget,
      systemOverlayStyle: appbarBrightness,
      backgroundColor: appBarColor,
      elevation: elevation == 0 ? 1 : elevation,
      //解决ios上webview侧滑闪烁问题
      shadowColor: elevation == 0 ? Colors.transparent : null,
      centerTitle: true,
      titleSpacing: titleSpacing,
      actions: bannerRightAction,
      automaticallyImplyLeading: hasAppBarLeading,
      leading: hasAppBarLeading
          ? leading ?? UiUtils.getBannerBackBtn(context)
          : null,
      bottom: appBarBottom,
    );
  }
  Widget? footer;
  if (bottomNavigationBar != null) {
    footer = Container(
      color: bottomNavigationBarBg ?? Colors.white,
      child: SafeArea(
        child: bottomNavigationBar,
      ),
    );
  }
  return KeyboardDismisser(
    gestures: isIngoreKeyboardDismisser ? [] : LibUtils.getGestureType(),
    child: Scaffold(
      appBar: appbar,
      key: key,
      body: body,
      extendBodyBehindAppBar: extendBodyBehindAppBar,
      endDrawer: endDrawer,
      endDrawerEnableOpenDragGesture: endDrawerEnableOpenDragGesture,
      drawer: startDrawer,
      drawerEnableOpenDragGesture: drawerEnableOpenDragGesture,
      backgroundColor: bg,
      floatingActionButton: floatingActionButton,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      bottomNavigationBar: footer,
    ),
  );
}

abstract class BasePageState<T extends StatefulWidget, R extends BaseLogic,
    F extends BaseStateMixin> extends State<T> {
  late R logic;
  late F state;

  @override
  void initState() {
    super.initState();
    var log = createLogic();
    logic = Get.put(log,
        tag: log.data.isSinglePage ? null : log.data.pageTag, permanent: true);
    state = logic.data as F;
  }

  @override
  Widget build(BuildContext context) {
    if (state.isGetBuilder) {
      return GetBuilder(
          init: logic,
          tag: logic.data.isSinglePage ? null : logic.data.pageTag,
          builder: (c) {
            return buildPage(context);
          });
    } else {
      return buildPage(context);
    }
  }

  @override
  void dispose() {
    super.dispose();
    Get.delete<R>(
        tag: logic.data.isSinglePage ? null : logic.data.pageTag, force: true);
  }

  R createLogic();

  Widget buildPage(BuildContext context);
}
