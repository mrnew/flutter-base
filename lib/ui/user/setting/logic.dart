import 'package:flutter/material.dart';
import 'package:flutter_cupertino_datetime_picker/flutter_cupertino_datetime_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:package_info/package_info.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/util/cache_util.dart';
import 'package:wameiji/ui/util/dialog_util.dart';
import 'package:wameiji/ui/util/image_upload.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'state.dart';

class SettingLogic extends BaseLogic {
  final SettingState data = SettingState();

  @override
  void onInit() async {
    super.onInit();
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    data.version.value = packageInfo.version;
    data.cache.value = await loadCache();
    var jpush = await CacheManager.getInstance()
        .get(false, 'isNotificationEnabled', null);
    data.isEnableNotify.value = jpush == null;
  }

  void switchNotify() {
    data.isEnableNotify.value = !data.isEnableNotify.value;
    if (!data.isEnableNotify.value) {
      CacheManager.getInstance().put(false, 'isNotificationEnabled', 'true');
    } else {
      CacheManager.getInstance().remove(false, 'isNotificationEnabled');
    }
  }

  void onLogOut(BuildContext context) {
    DialogUtil.showMyDialog('确定注销？').then((value) {
      if (value == 1) {
        UiUtils.logout(context);
      }
    });
  }

  void onShare() {}

  void onModifyNick(BuildContext context) async {
    var controller = TextEditingController();
    controller.text = UserManager.user.value?.nickname ?? '';
    var value = await DialogUtil.showMyEditDialog(controller,
        title: '请输入昵称', maxLength: 20);
    if (!UiUtils.isEmpty(value)) {
      var param = {
        'n': 'Sig.Front.User.AppUserSetting.ProfileEditAction',
        'nickname': value,
      };
      HttpClient.instance.request(
        param,
        isGet: false,
        cancelToken: cancelToken,
        isShowLoading: true,
        success: (Object? data) {
          UserManager.onRefrshUser();
        },
      );
    }
  }

  void onChangeSex(BuildContext context, String sex) {
    var param = {
      'n': 'Sig.Front.User.AppUserSetting.ProfileEditAction',
      'sex': sex,
    };
    HttpClient.instance.request(
      param,
      isGet: false,
      cancelToken: cancelToken,
      isShowLoading: true,
      success: (Object? entity) {
        UserManager.onRefrshUser();
      },
    );
  }

  void onBirth(BuildContext context) async {
    DatePicker.showDatePicker(
      context,
      minDateTime: DateTime(1900),
      maxDateTime: DateTime.now(),
      initialDateTime: UserManager.user.value?.birthday != null &&
              !UserManager.user.value!.birthday!.startsWith('0000')
          ? DateTime.tryParse(
                  UserManager.user.value!.birthday!.replaceAll('.', '-')) ??
              DateTime.now()
          : DateTime.now(),
      locale: DateTimePickerLocale.zh_cn,
      dateFormat: 'yyyy-MM-dd',
      onConfirm: (dateTime, selectedIndex) async {
        await Navigator.maybePop(context);
        var param = {
          'n': 'Sig.Front.User.AppUserSetting.ProfileEditAction',
          'birthday': dateTime.toString().substring(0, 10),
        };
        HttpClient.instance.request(
          param,
          isGet: false,
          cancelToken: cancelToken,
          isShowLoading: true,
          success: (Object? entity) {
            UserManager.onRefrshUser();
          },
        );
      },
    );
  }

  void onClearCache(BuildContext context) async {
    var value = await DialogUtil.showMyDialog('确定清理缓存？');
    if (value == 1) {
      var isSuc = await clearCache();
      if (isSuc) {
        data.cache.value = '0K';
        update();
      }
    }
  }

  void onSelAvatar(BuildContext context) async {
    var assets = await LibUtils.selPic(context, 1);
    if (assets == null || assets.isEmpty) return;
    //剪切图片
    var file = await assets[0].file;
    if (file == null) return;
    var path = file.absolute.path;
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: path,
      aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: '剪切图片',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.square,
            lockAspectRatio: true),
        IOSUiSettings()
      ],
    );
    // 压缩上传
    LibUtils.showLoading();
    var value;
    try {
      value = await ImageUploader(null, false, file: croppedFile).start();
    } catch (error) {
      if (error is HttpException) {
        LibUtils.showToast(error.msg);
        LibUtils.hideLoading();
        return;
      }
    }
    var param = {
      'n': 'Sig.Front.User.AppUserSetting.ProfileEditAction',
      'avatar': value,
    };
    HttpClient.instance.request(param, isGet: false, cancelToken: cancelToken,
        success: (Object? entity) {
      LibUtils.hideLoading();
      UserManager.onRefrshUser();
    }, error: (HttpException e) {
      LibUtils.hideLoading();
    });
  }

  void deleteUser() {
    DialogUtil.showMyDialog('确定注销账号？').then((value) {
      if (value == 1) {}
    });
  }
}
