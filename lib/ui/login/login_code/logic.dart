import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/check_util.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/user_entity.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/event_bus.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import '../../../config.dart';
import 'state.dart';

class LoginCodeLogic extends BaseLogic {
  final LoginCodeState data = LoginCodeState();

  @override
  void onInit() async {
    super.onInit();
    var lastName = await CacheManager.getInstance().get(false, 'lastName', '');
    data.unameController.text = lastName!;
    data.unameController.selection = TextSelection.fromPosition(TextPosition(
        affinity: TextAffinity.downstream, offset: lastName.length));
  }

  void onSms() {
    var mobile = data.unameController.text;
    if (UiUtils.isEmpty(mobile)) {
      LibUtils.showToast("请输入手机号");
      Future.delayed(Duration.zero, () {
        if (data.ctr != null && !isClosed) {
          data.ctr!.reset();
        }
      });
      return;
    }
    if (!CheckUtil.isPhone(mobile)) {
      LibUtils.showToast("请正确填写手机号码");
      Future.delayed(Duration.zero, () {
        if (data.ctr != null && !isClosed) {
          data.ctr!.reset();
        }
      });
      return;
    }
    HttpClient.instance.request<Object>({
      'n': 'Sig.Front.AppApi.SendVerifyCodeAction',
      'type': 'SMS',
      'user': mobile,
    }, isGet: false, cancelToken: cancelToken, successText: "短信验证码已发送",
        error: (HttpException e) {
      if (data.ctr != null) {
        data.ctr!.reset();
      }
    });
  }

  void onLogin(BuildContext context) {
    LibUtils.hideKeyboard(context);
    if (!data.isArgree.value) {
      LibUtils.showToast("确认阅读并同意用户条款和隐私政策");
      return;
    }
    var name = data.unameController.text;
    var code = data.codeController.text;
    if (name.isEmpty) {
      LibUtils.showToast("请输入手机号");
      return;
    }
    if (code.isEmpty) {
      LibUtils.showToast("请输入验证码");
      return;
    }
    UserEntity user = UserEntity();
    user.localId = name;
    if (!UiUtils.isEmpty(name)) {
      CacheManager.getInstance().put(false, 'lastName', name);
    }
    UserManager.save(user);
    LibUtils.eventBus.fire(const LoginEvent());
    LibUtils.showToast("登录成功");
    LibUtils.hideLoading();
    if (GlobalConfig.isMustLogin) {
      Get.offNamed('main_page');
    } else {
      Get.back();
    }
    return;
  }
}
