import 'package:flutter/material.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/widget/business/form.dart';

import 'logic.dart';
import 'state.dart';

class SecondPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<SecondPage, SecondLogic, SecondState> {
  @override
  SecondLogic createLogic() {
    return SecondLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(context, logic, () {
      var children = <Widget>[];
      children.add(const SizedBox(height: 100,));
      children.add(const FormTitleValue(title: '标题',value: '显示',));
      children.add(const FormTitleSel(title: '标题',value: '显示',));
      children.add(const FormTitleEdit(title: '标题',));
      children.add(const FormTitleValueTap(title: '标题',value: '显示',));
      children.add(const FormTitleImage(title: '标题',urls: ['显示','显示','显示','显示','显示',],));
      children.add(const FormTitleSelImage(title: '标题',notice: '12313',urls: ['显示','显示','显示','显示','显示',],max: 9,));
      return Container(
        color: Colors.white,
        child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: children,
            )),
      );
    });
  }
}
