import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/widget/delay_visibility.dart';
import 'package:mrnew_libary/widget/web_progress.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'logic.dart';
import 'state.dart';

class WebPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<WebPage, WebLogic, WebState> {
  @override
  WebLogic createLogic() {
    return WebLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return buildView(logic, state);
  }
}

Widget buildView(WebLogic logic, WebState state) {
  var actions = <Widget>[];
  return myPage(Get.context!, logic, () {
    return getWeb(state);
  }, title: state.title ?? '详情', bannerRightAction: actions);
}

///获取通用web
getWeb(WebState state) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      WebProgress(state.progressController),
      Expanded(
        child: Stack(
          children: [
            Positioned.fill(
              child: WebViewWidget(
                controller: state.webController,
              ),
            ),
            const Positioned.fill(
                child: DelayVisiBility(
              child: DecoratedBox(
                decoration: BoxDecoration(color: Colors.white),
              ),
            ))
          ],
        ),
      )
    ],
  );
}
