import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:mrnew_libary/widget/js/js_bridge.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

typedef ExtandJsBridgeCallBack = void Function(dynamic func, dynamic param);

class PageUtils {
  ///打开普通web
  static void startWeb(String? url, String? title) {
    if (UiUtils.isEmpty(url)) return;
    Uri u = Uri.parse(url!);
    if (UiUtils.isEmpty(u.scheme)) {
      url = HttpClient.baseUrl + url;
    }
    Get.toNamed('web_page',
        arguments: {
          'url': url,
          'title': title ?? '详情',
        },
        preventDuplicates: false);
  }


  static String? getParam(String url, String name) {
    try {
      Uri u = Uri.parse(url);
      return u.queryParameters[name];
    } catch (e) {
      return null;
    }
  }

  ///设置公用的js
  static void initJs(JsBridge jsBridge, BuildContext context,
      {ExtandJsBridgeCallBack? extandJs}) {
    jsBridge.registerHandler("callLocalApp", onCallBack: (callBackData, ret) {
      //{param: {id: 3666, url: /?n=Sig.Front.AppFront.ArticleDetailH5&id=3666}, func: openArticle}
      Map? map;
      if (callBackData is String) {
        map = json.decode(callBackData);
      } else {
        map = callBackData.data;
      }
      var param = map!['param'];
      var func = map['func'];
      if (func == null) return;
      if (func == 'newBrowser') {
        startWeb(param['url']?.toString(), null);
      }

      if (extandJs != null) {
        extandJs.call(func, param);
      }
      try {
        ret.call('调用成功');
      } catch (e) {}
    });
  }

  ///推送点击跳转
  static void startPushPage(int? type, dynamic param) {
    Future.delayed(Duration.zero, () {
      if (type == 1) {

      }
    });
  }

  static void startApp(String url, int type) async {
    String appName = '支付宝应用';
    if (type == 1) {
      appName = '淘宝应用';
    }
    try {
      if (GetPlatform.isIOS) {
        launch(url).then((value) {
          if (value == false) {
            LibUtils.showToast('请先安装$appName');
            return;
          }
        });
      } else {
        canLaunch(url).then((value) {
          if (value == false) {
            LibUtils.showToast('请先安装$appName');
            return;
          }
          launch(url);
        });
      }
    } catch (e) {
      LibUtils.showToast('请先安装$appName');
    }
  }
}
