import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:mrnew_libary/widget/icon_text.dart';
import 'package:wameiji/model/user_entity.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/util/page_utils.dart';
import 'package:wameiji/ui/util/ui_utils.dart';

import 'logic.dart';
import 'state.dart';

class SettingPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<SettingPage, SettingLogic, SettingState> {
  SettingLogic createLogic() {
    return SettingLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return buildView(context, logic, state);
  }
}

Widget buildView(BuildContext context, SettingLogic logic, SettingState state) {
  return myPage(context, logic, () {
    return Obx(() {
      var children = <Widget>[];
      if (UserManager.isLogin()) {
        UserEntity user = UserManager.user.value!;
        children.add(const Padding(
          padding: EdgeInsets.only(top: 10),
          child: MyStyle.divider,
        ));
        children.add(GestureDetector(
            onTap: () {
              logic.onSelAvatar(context);
            },
            child: _getAvatar(user, logic, state)));
        children.add(MyStyle.divider);
        children.add(GestureDetector(
            onTap: () {
              Get.toNamed('address_list_page', preventDuplicates: false);
            },
            behavior: HitTestBehavior.translucent,
            child: _getItem(Res.setting_icon1, '管理收货地址')));
        children.add(MyStyle.divider);
        children.add(const Padding(
          padding: EdgeInsets.only(top: 10),
          child: MyStyle.divider,
        ));
        children.add(GestureDetector(
            onTap: () {
              logic.onModifyNick(context);
            },
            behavior: HitTestBehavior.translucent,
            child: _getItem(Res.setting_icon2, '我的昵称', value: user.nickname)));
        children.add(MyStyle.divider);
        children.add(_getSexItem(context, user, logic, state));
        children.add(MyStyle.divider);
        children.add(GestureDetector(
            onTap: () {
              logic.onBirth(context);
            },
            behavior: HitTestBehavior.translucent,
            child: _getItem(Res.setting_icon4, '我的生日', value: user.birthday)));
        children.add(MyStyle.divider);
        children.add(const Padding(
          padding: EdgeInsets.only(top: 10),
          child: MyStyle.divider,
        ));
        children.add(GestureDetector(
            onTap: () {
            },
            behavior: HitTestBehavior.translucent,
            child: _getItem(Res.setting_icon5, '我的手机号', value: user.mobile)));
        children.add(MyStyle.divider);
        children.add(GestureDetector(
          onTap: () {
          },
          behavior: HitTestBehavior.translucent,
          child: _getItem(Res.setting_icon6, '我的邮箱', value: user.email),
        ));
        children.add(MyStyle.divider);
        children.add(GestureDetector(
            onTap: () {
              Get.toNamed('modify_pwd_page', preventDuplicates: false);
            },
            behavior: HitTestBehavior.translucent,
            child: _getItem(Res.setting_icon7, '修改密码')));
        children.add(MyStyle.divider);
      }
      children.add(const Padding(
        padding: EdgeInsets.only(top: 10),
        child: MyStyle.divider,
      ));
      children.add(GestureDetector(
          onTap: () {
            logic.onClearCache(context);
          },
          behavior: HitTestBehavior.translucent,
          child: Obx(() =>
              _getItem(Res.setting_icon8, '清除缓存', value: state.cache.value))));
      children.add(MyStyle.divider);
      children.add(GestureDetector(
          onTap: () {
            Get.toNamed('about_page', preventDuplicates: false);
          },
          behavior: HitTestBehavior.translucent,
          child: _getItem(Res.setting_icon9, '关于我们')));
      children.add(MyStyle.divider);
      children.add(GestureDetector(
          onTap: () {
            PageUtils.startWeb("https://www.wenjuan.com/s/zUBbA36/", "问题反馈");
          },
          behavior: HitTestBehavior.translucent,
          child: _getItem(Res.setting_icon0, '问题反馈')));
      children.add(MyStyle.divider);
      children.add(GestureDetector(
          onTap: () {
            logic.onShare();
          },
          behavior: HitTestBehavior.translucent,
          child: _getItem(Res.setting_icon10, '分享App')));
      children.add(MyStyle.divider);
      children.add(GestureDetector(
          onTap: () {
          },
          behavior: HitTestBehavior.translucent,
          child: _getItem(Res.setting_icon11, '前往评价')));
      children.add(MyStyle.divider);
      if (UserManager.isLogin()) {
        children.add(GestureDetector(
            onTap: () {
              logic.deleteUser();
            },
            behavior: HitTestBehavior.translucent,
            child: _getItem(Res.setting_icon15, '注销账号')));
        children.add(MyStyle.divider);
      }
      if (GetPlatform.isAndroid) {
        children.add(Container(
          height: 50,
          color: Colors.white,
          padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
          child: Row(
            children: <Widget>[
              Container(
                  width: 50,
                  alignment: Alignment.center,
                  child: const Image(
                    image: AssetImage(Res.setting_icon16),
                  )),
              const Text(
                '推送通知',
                style: MyText.textStyle14_5e616b,
              ),
              const Spacer(),
              CupertinoSwitch(
                value: state.isEnableNotify.value,
                onChanged: (value) {
                  logic.switchNotify();
                },
                activeColor: const Color(0xffd55b74),
              ),
            ],
          ),
        ));
        children.add(
          MyStyle.divider,
        );
      }
      children.add(GestureDetector(
          onTap: () {
            if (GetPlatform.isAndroid) {
              UiUtils.checkUpgradle(context, true);
            }
          },
          behavior: HitTestBehavior.translucent,
          child: Obx(() => _getItem(Res.setting_icon12, '当前版本',
              value: state.version.value))));
      children.add(const Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: MyStyle.divider,
      ));
      return SingleChildScrollView(
        child: ListBody(
          children: children,
        ),
      );
    });
  },
      title: '设置',
      bottomNavigationBar:
          !UserManager.isLogin() ? null : _getFooter(context, logic, state),
      bottomNavigationBarBg: Colors.white);
}

Container _getAvatar(UserEntity user, SettingLogic logic, SettingState state) {
  return Container(
    color: Colors.white,
    padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
    child: Row(
      children: <Widget>[
        Container(
            width: 50,
            alignment: Alignment.center,
            child: const Image(image: AssetImage(Res.setting_icon0))),
        const Text(
          '我的头像',
          style: MyText.textStyle14_5e616b,
        ),
        const Spacer(),
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: ClipOval(
            child: UiUtils.getNetImageFit(
              user.avatar,
              fit: BoxFit.cover,
              cacheWidth: 48,
              width: 48,
              height: 48,
            ),
          ),
        ),
        const Image(image: AssetImage(Res.arrow_r))
      ],
    ),
  );
}

Container _getItem(String icon, String title,
    {String? value = '', bool noArrow = false}) {
  return Container(
    constraints: const BoxConstraints(minHeight: 50),
    color: Colors.white,
    padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
    child: Row(
      children: <Widget>[
        Container(
            width: 50,
            alignment: Alignment.center,
            child: Image(
              image: AssetImage(icon),
            )),
        Text(
          title,
          style: MyText.textStyle14_5e616b,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              value ?? '',
              style: MyText.textStyle14_202228,
              maxLines: 1,
              textAlign: TextAlign.right,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        noArrow
            ? const SizedBox(
                width: 0,
              )
            : const Padding(
                padding: EdgeInsets.only(left: 10),
                child: Image(image: AssetImage(Res.arrow_r)),
              )
      ],
    ),
  );
}

Container _getSexItem(BuildContext context, UserEntity user, SettingLogic logic,
    SettingState state) {
  return Container(
    constraints: const BoxConstraints(minHeight: 50),
    color: Colors.white,
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    child: Row(
      children: <Widget>[
        Container(
            width: 30,
            alignment: Alignment.centerLeft,
            child: const Image(image: AssetImage(Res.setting_icon3))),
        const Text(
          '我的性别',
          style: MyText.textStyle14_5e616b,
        ),
        const Spacer(),
        SizedBox(
            width: 70,
            height: 28,
            child: GestureDetector(
              onTap: () {
                logic.onChangeSex(context, '男');
              },
              behavior: HitTestBehavior.translucent,
              child: user.sex != '女'
                  ? const Image(image: AssetImage(Res.set_sex_0_))
                  : const Image(image: AssetImage(Res.set_sex_0)),
            )),
        Container(
            width: 70,
            height: 28,
            margin: const EdgeInsets.only(left: 10),
            child: GestureDetector(
                onTap: () {
                  logic.onChangeSex(context, '女');
                },
                behavior: HitTestBehavior.translucent,
                child: user.sex != '女'
                    ? const Image(image: AssetImage(Res.set_sex_1))
                    : const Image(image: AssetImage(Res.set_sex_1_)))),
      ],
    ),
  );
}

Widget _getFooter(
    BuildContext context, SettingLogic logic, SettingState state) {
  return GestureDetector(
    onTap: () {
      logic.onLogOut(context);
    },
    behavior: HitTestBehavior.translucent,
    child: Container(
      height: 50,
      color: const Color(0xffd55b74),
      padding: const EdgeInsets.only(left: 20),
      child: const IconText(
        '退出当前账号',
        style: MyText.textStyle14_fff,
        padding: 15,
        icon: Image(image: AssetImage(Res.setting_icon13)),
      ),
    ),
  );
}
