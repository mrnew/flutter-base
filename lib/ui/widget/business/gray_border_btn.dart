import 'package:flutter/material.dart';

class GrayBorderBtn extends StatelessWidget {
  final String? text;
  final Function? onPress;
  final double height;
  final double? width;
  final double fontSize;
  final Color borderColor;
  final Color textColor;

  const GrayBorderBtn({
    this.text,
    this.onPress,
    this.width,
    this.height = 28,
    this.fontSize = 12,
    this.borderColor = const Color(0xff666666),
    this.textColor = Colors.black,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPress?.call();
      },
      behavior: HitTestBehavior.translucent,
      child: Center(
        child: IntrinsicWidth(
          child: Container(
            height: height,
            width: width,
            padding: const EdgeInsets.symmetric(horizontal: 12),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(24)),
                border: Border.all(color: borderColor, width: 1)),
            child: Text(
              text ?? '',
              style: TextStyle(color: textColor, fontSize: fontSize),
            ),
          ),
        ),
      ),
    );
  }
}
