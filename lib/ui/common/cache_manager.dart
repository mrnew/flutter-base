import 'package:sqflite/sqflite.dart';
import 'package:wameiji/ui/common/user_manager.dart';

class CacheManager {
  static String? uid;

  CacheManager._();

  static Database? db;
  static CacheManager? _instance;
  static const String _global_user = 'global_user';

  static CacheManager getInstance() {
    if (_instance == null) {
      _instance = CacheManager._();
    }
    return _instance!;
  }

  Future<void> openDb() async {
    if (db == null || !db!.isOpen) {
      db = await openDatabase('my_db.db');
      await db!.execute(
          'CREATE TABLE IF NOT EXISTS table_cache  (account VARCHAR ,key VARCHAR,value VARCHAR)');
      await db!.execute(
          'CREATE TABLE IF NOT EXISTS table_trans_cache (_id INTEGER PRIMARY KEY AUTOINCREMENT,uniKey VARCHAR ,title VARCHAR,desc VARCHAR)');
    }
  }

  Future<String?> get(bool isBindUser, String key, String? defaultValue) async {
    if (db == null || !db!.isOpen) {
      await openDb();
    }
    String? uid;
    if (isBindUser) {
      if (CacheManager.uid != null) {
        uid = CacheManager.uid;
      } else {
        return defaultValue;
      }
    } else {
      uid = _global_user;
    }
    var result = await db!.rawQuery(
        'SELECT value FROM table_cache where key = ? and account = ?',
        [key, uid!]);
    if (result.isEmpty) return defaultValue;
    return result[0]['value'] as String?;
  }

  Future<bool> put(bool isBindUser, String key, String? value) async {
    if (db == null || !db!.isOpen) {
      await openDb();
    }
    String? uid;
    if (isBindUser) {
      if (CacheManager.uid != null) {
        uid = CacheManager.uid;
      } else {
        return false;
      }
    } else {
      uid = _global_user;
    }
    await db!.rawDelete(
        'DELETE FROM  table_cache where key = ? and account = ?', [key, uid!]);
    var count = await db!.rawInsert(
        'INSERT INTO table_cache(key,value,account) values(?,?,?)',
        [key, value ?? '', uid]);
    return count > 0;
  }

  Future<bool> remove(bool isBindUser, String key) async {
    if (db == null || !db!.isOpen) {
      await openDb();
    }
    String? uid;
    if (isBindUser) {
      if (CacheManager.uid != null) {
        uid = CacheManager.uid;
      } else {
        return false;
      }
    } else {
      uid = _global_user;
    }
    await db!.rawDelete(
        'DELETE FROM  table_cache where key = ? and account = ?', [key, uid!]);
    return true;
  }

  Future<bool> removeAll(bool isBindUser) async {
    if (db == null || !db!.isOpen) {
      await openDb();
    }
    String? uid;
    if (isBindUser) {
      if (CacheManager.uid != null) {
        uid = CacheManager.uid;
      } else {
        return false;
      }
    } else {
      uid = _global_user;
    }
    await db!.rawDelete('DELETE FROM table_cache where account = ?', [uid!]);
    return true;
  }

}
