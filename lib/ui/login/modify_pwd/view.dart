import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:mrnew_libary/widget/divider.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/common_res.dart';

import 'logic.dart';
import 'state.dart';

class ModifyPwdPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State
    extends BasePageState<ModifyPwdPage, ModifyPwdLogic, ModifyPwdState> {
  @override
  ModifyPwdLogic createLogic() {
    return ModifyPwdLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return myPage(context, logic, () {
      return SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: state.oldController,
                obscureText: true,
                style: const TextStyle(
                  fontSize: 15,
                ),
                textAlignVertical: TextAlignVertical.center,
                decoration: const InputDecoration(
                    hintText: "请输入原始密码",
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    contentPadding: EdgeInsets.symmetric(vertical: 20)),
              ),
            ),
            const MyDivider(
              indent: 15,
              endIndent: 15,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: state.pwdController,
                style: const TextStyle(
                  fontSize: 15,
                ),
                textAlignVertical: TextAlignVertical.center,
                decoration: const InputDecoration(
                    hintText: "请输入新密码",
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    contentPadding: EdgeInsets.symmetric(vertical: 20)),
                obscureText: true,
              ),
            ),
            const MyDivider(
              indent: 15,
              endIndent: 15,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: state.pwdSureController,
                textAlignVertical: TextAlignVertical.center,
                style: const TextStyle(
                  fontSize: 15,
                ),
                decoration: const InputDecoration(
                    hintText: "请再次输入新密码",
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    contentPadding: EdgeInsets.symmetric(vertical: 20)),
                obscureText: true,
              ),
            ),
            const MyDivider(
              indent: 15,
              endIndent: 15,
            ),
            Center(
              child: Container(
                margin: const EdgeInsets.only(top: 70),
                width: 240,
                height: 46,
                child: TextButton(
                  child: const Text(
                    '确认',
                    style: MyText.textStyle15_fff,
                  ),
                  style: TextButton.styleFrom(
                      backgroundColor: MyColor.globalBtnColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                  onPressed: () {
                    logic.onSubmit();
                  },
                ),
              ),
            ),
          ],
        ),
      );
    }, title: '修改密码', bg: Colors.white);
  }
}
