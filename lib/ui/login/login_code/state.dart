import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wameiji/ui/base/page.dart';

class LoginCodeState with BaseStateMixin {
  TextEditingController unameController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  RxBool isArgree = false.obs;
  AnimationController? ctr;
}
