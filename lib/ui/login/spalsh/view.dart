import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/base/page.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/util/dialog_util.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:wameiji/ui/widget/animated_splash.dart';

import 'logic.dart';
import 'state.dart';

class SpalshPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends BasePageState<SpalshPage, SpalshLogic, SpalshState> {
  @override
  SpalshLogic createLogic() {
    return SpalshLogic();
  }

  @override
  Widget buildPage(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Obx(() {
          if (!state.isReady.value || state.entity == null) {
            return AnimatedSplash(
              customFunction: (BuildContext context) async {
                if (state.entity != null) return;
                var str = await CacheManager.getInstance()
                    .get(false, "splash_dialog", null);
                if (str == null) {
                  var ret = await DialogUtil.showUserNotceDialog(context);
                  if (ret == 0 || ret == null) {
                    //关闭程序
                    LibUtils.closeApp();
                    return;
                  } else {
                    CacheManager.getInstance()
                        .put(false, "splash_dialog", 'true');
                  }
                }
                logic.goMain();
              },
              duration: GlobalConfig.hasSplash ? 3000 : 10,
              type: AnimatedSplashType.StaticDuration,
            );
          }

          return Stack(alignment: Alignment.topCenter, children: [
            Positioned.fill(
                bottom: GlobalConfig.isTable ? 0 : 130,
                child: GestureDetector(
                  onTap: () {
                    logic.goH5();
                  },
                  behavior: HitTestBehavior.translucent,
                  child: UiUtils.getNetImageFit(state.entity!.img,
                      fit: BoxFit.cover),
                )),
            Positioned(
                right: 15,
                width: 95,
                height: 30,
                top: 60,
                child: GestureDetector(
                  onTap: () {
                    logic.goMain();
                  },
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Color(0x88000000),
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                    ),
                    alignment: Alignment.center,
                    child: Obx(() {
                      return Text(
                        "跳过（${state.count.value}s）",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: MyText.textStyle14_fff,
                      );
                    }),
                  ),
                )),
            if (!GlobalConfig.isTable)
              Positioned(
                  bottom: 0,
                  height: 130,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Image(
                          image: AssetImage(Res.splash_logo),
                          height: 40,
                          fit: BoxFit.contain,
                          gaplessPlayback: true,
                        ),
                      ),
                      Text(
                        'Copyright © 2020-2022 挖煤姬 meruki.cn 版权所有',
                        style: TextStyle(
                          fontSize: 11,
                          color: Color(0xff9B9B9B),
                        ),
                      ),
                    ],
                  ))
          ]);
        }));
  }
}
