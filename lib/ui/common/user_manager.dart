import 'dart:convert';

import 'package:get/get.dart';
import 'package:wameiji/generated/json/base/json_convert_content.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/user_entity.dart';
import 'package:wameiji/ui/common/cache_manager.dart';

class UserManager {
  static Rx<UserEntity?> user = Rx(null);

  static init() async {
    user.value = await getLoginModel();
  }

  static logout() {
    user.value = null;
    user.refresh();
  }

  static onRefrshUser() {
    HttpClient.instance.request<UserEntity>({
      'n': 'Sig.Front.User.AppUserSetting.GetUserInfo',
    }, isGet: false, success: (UserEntity? data) async {
      if (user.value != null) data!.localId = user.value!.localId;
      CacheManager.getInstance().put(false, 'user', jsonEncode(data));
      user.value = data;
      user.refresh();
    });
  }

  static save(UserEntity data) {
    if (user.value != null) data.localId = user.value!.localId;
    CacheManager.getInstance().put(false, 'user', jsonEncode(data));
    user.value = data;
    user.refresh();
  }

  static bool isLogin() {
    return user.value != null;
  }

  static Future<UserEntity?> getLoginModel() async {
    //初始化用户信息
    var value = await CacheManager.getInstance().get(false, 'user', null);
    if (value != null) {
      var data =
          JsonConvert.fromJsonAsT(jsonDecode(value), (UserEntity).toString());
      return data;
    }
    return null;
  }
}
