import 'package:get/get.dart';
import 'package:wameiji/ui/base/page.dart';

class SettingState with BaseStateMixin {
  RxString version = ''.obs;
  RxString cache = ''.obs;
  RxBool isEnableNotify = true.obs;

  SettingState() {
    ///Initialize variables
  }
}
