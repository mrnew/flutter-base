import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:leak_detector/leak_detector.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/common/user_manager.dart';
import 'package:wameiji/ui/login/forget/view.dart';
import 'package:wameiji/ui/login/login_sys/view.dart';
import 'package:wameiji/ui/login/modify_pwd/view.dart';
import 'package:wameiji/ui/login/register/view.dart';
import 'package:wameiji/ui/login/spalsh/view.dart';
import 'package:wameiji/ui/main/view.dart';
import 'package:wameiji/ui/user/about/view.dart';
import 'package:wameiji/ui/user/setting/view.dart';
import 'package:wameiji/ui/util/ui_utils.dart';
import 'package:wameiji/ui/web/view.dart';

import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

List<GetPage> pages = [
  GetPage(
    name: '/',
    participatesInRootNavigator: true,
    page: () => SpalshPage(),
    transition: Transition.fadeIn,
  ),
  GetPage(
    name: '/main_page',
    page: () => MainPage(),
    transition: Transition.fadeIn,
  ),
  GetPage(
    name: '/login_page',
    page: () => LoginSysPage(),
    transition: Transition.fadeIn,
  ),
  GetPage(
    name: '/register_page',
    page: () => RegisterPage(),
    transition: Transition.fadeIn,
  ),
  GetPage(
    name: '/forget_page',
    page: () => ForgetPage(),
  ),
  GetPage(
    name: '/modify_pwd_page',
    page: () => ModifyPwdPage(),
  ),
  GetPage(
    name: '/about_page',
    page: () => AboutPage(),
  ),
  GetPage(
    name: '/web_page',
    page: () => WebPage(),
  ),
  GetPage(
    name: '/setting_page',
    page: () => SettingPage(),
  ),
];

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

Widget createApp() {
  return RefreshConfiguration(
    //下拉刷新配置
    headerBuilder: () => const MaterialClassicHeader(),
    footerBuilder: () => const ClassicFooter(),
    footerTriggerDistance: 400, //预加载距离
    child: GetMaterialApp(
      title: '框架应用',
      enableLog: GlobalConfig.isTest,
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          child: child!,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      theme: ThemeData(
          fontFamily: 'PingFang SC',
          brightness: Brightness.light,
          primaryColor: MyColor.globalThemeColor,
          hintColor: const Color(0xffcccccc)),
      localizationsDelegates: const [
        RefreshLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: const [
        Locale('zh'),
        Locale('en'),
      ],
      locale: const Locale('zh'),
      navigatorObservers: [
        routeObserver,
        if (GlobalConfig.isTest && GlobalConfig.isCheckMemory)
          LeakNavigatorObserver(
            //返回false则不会校验这个页面.
            shouldCheck: (route) {
              return route.settings.name != null && route.settings.name != '/';
            },
          ),
      ],
      getPages: pages,
      defaultTransition: Transition.rightToLeft,
    ),
  );
}

class LoginRouteMiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    if (!UserManager.isLogin()) {
      return const RouteSettings(name: '/login_page');
    }
    return super.redirect(route);
  }
}
