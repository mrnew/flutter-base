import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_upgrade/flutter_app_upgrade.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart' as cache;
import 'package:flutter_cupertino_datetime_picker/flutter_cupertino_datetime_picker.dart';
import 'package:get/get.dart';
import "package:intl/intl.dart";
import 'package:mrnew_libary/util/lib_util.dart';
import 'package:mrnew_libary/widget/photo_view.dart';
import 'package:wameiji/config.dart';
import 'package:wameiji/http/http_client.dart';
import 'package:wameiji/model/apk_info_entity.dart';
import 'package:wameiji/res.dart';
import 'package:wameiji/ui/common/cache_manager.dart';
import 'package:wameiji/ui/common/common_res.dart';
import 'package:wameiji/ui/common/event_bus.dart';
import 'package:wameiji/ui/common/user_manager.dart';

typedef DateValueCallback(DateTime dateTime, List<int> selectedIndex);

class DefaultCacheManager extends cache.CacheManager {
  static const key = 'libCachedImageData1';
  static DefaultCacheManager? _instance;

  factory DefaultCacheManager() {
    _instance ??= DefaultCacheManager._();
    return _instance!;
  }

  DefaultCacheManager._()
      : super(cache.Config(
          key,
          stalePeriod: const Duration(days: 3),
          maxNrOfCacheObjects: 120,
        ));
}

class UiUtils {
  static double? _screenSize;

  static int getImageCountWidth(BuildContext? context, double rate) {
    if (_screenSize == null) {
      _screenSize = MediaQuery.of(context!).size.width *
          MediaQuery.of(context).devicePixelRatio;
    }
    return (_screenSize! * rate).toInt();
  }

  static Widget getNetImageGoods(
    String? url, {
    double? cacheWidthRate,
    int? cacheWidth,
    bool useWhitePlaceholder = false,
    double? width,
    double? height,
  }) {
    return getNetImageFit(
      url,
      fit: BoxFit.cover,
      cacheWidthRate: cacheWidthRate,
      cacheWidth: cacheWidth,
      useWhitePlaceholder: useWhitePlaceholder,
      width: width,
      height: height,
    );
  }

  static Widget whiteBox = const DecoratedBox(
    decoration: BoxDecoration(color: Colors.white),
  );
  static Widget avatarPlaceholder = const Image(
    image: AssetImage(Res.avatar_defalut_bg),
    fit: BoxFit.fill,
  );

  static Widget getNetImageFit(
    String? url, {
    BoxFit fit = BoxFit.cover,
    Widget? placeholder,
    bool hasPlaceholder = true,
    bool useWhitePlaceholder = false,
    double? cacheWidthRate,
    int? cacheWidth,
    Duration fadeInDuration = const Duration(milliseconds: 300),
    Duration fadeOutDuration = const Duration(milliseconds: 600),
    double? width,
    double? height,
  }) {
    if (url == null) url = '';
    if (!url.startsWith('http')) {
      url = HttpClient.baseUrl + url;
    }
    double ratio = MediaQuery.of(Get.context!).devicePixelRatio;
    if (cacheWidth != null) {
      cacheWidth = (cacheWidth * ratio).toInt();
    } else if (cacheWidthRate != null) {
      cacheWidth = getImageCountWidth(Get.context, cacheWidthRate);
    }

    if (hasPlaceholder) {
      if (placeholder != null) {
        placeholder = Center(
          child: placeholder,
        );
      } else {
        placeholder = useWhitePlaceholder == true
            ? whiteBox
            : const DecoratedBox(
                decoration: BoxDecoration(color: Color(0xfff7f7f7)),
              );
      }
    }
    return CachedNetworkImage(
      imageUrl: url,
      cacheManager: DefaultCacheManager(),
      memCacheWidth: cacheWidth,
      width: width,
      height: height,
      fadeInDuration: fadeInDuration,
      fadeOutDuration: fadeOutDuration,
      placeholder: (context, url) {
        return placeholder ??
            const SizedBox(
              width: 1,
              height: 1,
            );
      },
      errorWidget: (context, url, error) {
        return placeholder ??
            const SizedBox(
              width: 1,
              height: 1,
            );
      },
      fit: fit,
    );
  }

  static bool checkLogin() {
    if (UserManager.user.value == null) {
      goLogin();
      return false;
    }
    return true;
  }

  static void goLogin() {
    Get.toNamed('login_page', preventDuplicates: false);
  }

  static Widget getBannerBackBtn(BuildContext context,
      {bool useWhite = false}) {
    if (useWhite) {
      return IconButton(
        icon: const Image(image: AssetImage(Res.back_icon_white)),
        onPressed: () {
          Get.back();
        },
      );
    }
    return IconButton(
      icon: const Image(image: AssetImage(Res.back_icon)),
      onPressed: () {
        Get.back();
      },
    );
  }

  static Widget getBannerImageAction(String value, void Function()? onPressed,
      {Key? key}) {
    return SizedBox(
      width: 60,
      child: GestureDetector(
        onTap: onPressed,
        behavior: HitTestBehavior.translucent,
        child: Image.asset(
          value,
          key: key,
          gaplessPlayback: true,
        ),
      ),
    );
  }

  static Widget getBannerAction(String value, void Function()? onPressed) {
    return GestureDetector(
      onTap: onPressed,
      behavior: HitTestBehavior.translucent,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        height: 28,
        alignment: Alignment.center,
        child: Text(
          value,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 14,
            color: Color(0xff333333),
          ),
        ),
      ),
    );
  }

  static bool isEmpty(String? string) {
    return string == null || string.length == 0;
  }

  static String formatMoney(dynamic string,
      {bool hasDivder = false, bool hasUnit = true, String unit = '元'}) {
    if (string == null) return hasUnit ? ' 元' : ' ';
    try {
      var num = double.parse(string.toString());
      NumberFormat format =
          NumberFormat(hasDivder == true ? "###,##0.##" : "#####0.##");
      if (hasUnit) {
        return format.format(num) + unit;
      } else {
        return format.format(num);
      }
    } catch (e) {
      return string.toString();
    }
  }

  static String formatNumber(dynamic string, {int decimalLength = 2}) {
    if (string == null) return '';
    try {
      var str = '#####0';
      if (decimalLength > 0) {
        str += '.';
        for (var i = 0; i < decimalLength; ++i) {
          str += '#';
        }
      }
      var num = double.parse(string.toString());
      NumberFormat format = NumberFormat(str);

      return format.format(num);
    } catch (e) {
      return string.toString();
    }
  }

  static Future<bool> logout(
    BuildContext? context,
  ) async {
    LibUtils.showLoading(text: '注销中...');
    try {
      await CacheManager.getInstance().removeAll(true);
    } catch (e) {}
    await CacheManager.getInstance().remove(false, 'user');
    UserManager.logout();
    LibUtils.eventBus.fire(const LogoutEvent());

    if (GlobalConfig.isMustLogin) {
      Get.offAllNamed('login_page');
    } else {
      Get.offNamedUntil('login_page', ModalRoute.withName('main_page'));
    }
    return true;
  }

  static void checkUpgradle(BuildContext context, bool isManual) async {
    if (isManual) {
      LibUtils.showLoading();
    }
    HttpClient.instance.request<ApkInfoEntity>({
      'n': 'Sig.Front.Front.GetAppUpgradeInfoMC',
      'v': GlobalConfig.version,
    }, isShowErrorMsg: false, success: (ApkInfoEntity? entity) {
      if (isManual) LibUtils.hideLoading();
      if (isEmpty(entity?.url) ||
          !isUpdate(entity?.v ?? '', GlobalConfig.version ?? '')) {
        if (isManual) LibUtils.showToast('当前版本已是最新版本！');
        return;
      }
      if (!isEmpty(entity?.url)) {
        AppUpgrade.appUpgrade(
          Get.context!,
          Future.value(AppUpgradeInfo(
              title: '发现新版本',
              contents: [entity?.content ?? ''],
              force: entity?.forceUpgrade == '1',
              apkDownloadUrl: entity!.url!)),
        );
      }
    }, error: (HttpException e) {
      if (isManual) {
        LibUtils.hideLoading();
        LibUtils.showToast(e.msg);
      }
    });
  }

  static bool isUpdate(String version, String local) {
    var localStrs = local.split(".");
    var remoteStrs = version.split(".");
    int count = max(localStrs.length, remoteStrs.length);
    for (int i = 0; i < count; i++) {
      if (i >= localStrs.length) {
        return true;
      }
      if (i >= remoteStrs.length) {
        return false;
      }
      int localInt = 0;
      int remoteInt = 0;
      try {
        localInt = int.parse(localStrs[i]);
      } catch (e) {
        return true;
      }
      try {
        remoteInt = int.parse(remoteStrs[i]);
      } catch (e) {
        return false;
      }
      if (localInt > remoteInt) return false;
      if (localInt < remoteInt) return true;
    }
    return false;
  }

  static void showImage(
      BuildContext context, List? galleryItems, final int index,
      {bool verticalGallery = false}) {
    if (galleryItems == null || galleryItems.isEmpty) return;
    galleryItems = galleryItems
        .where((element) => !isEmpty(element?.toString()))
        .map((element) {
      Uri u = Uri.parse(element);
      if (UiUtils.isEmpty(u.scheme)) {
        element = HttpClient.baseUrl + element;
      }
      return element;
    }).toList();
    openShowImage(context, galleryItems, index,
        verticalGallery: verticalGallery);
  }

  static int getTableCount({double? width, int perWidth = 180, int min = 2}) {
    if (!GlobalConfig.isTable) return min;
    return max((width ?? Get.width) ~/ perWidth, min).toInt();
  }

  static PreferredSizeWidget getGlobalTabBar(List<String> tabs,
      {Color color = MyColor.globalThemeColor, bool isScrollable = true}) {
    return PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: Container(
          height: 50,
          width: double.maxFinite,
          decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(color: MyColor.gray_e6, width: .5))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Theme(
              data: ThemeData(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
              ),
              child: TabBar(
                indicatorColor: color,
                labelStyle: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
                unselectedLabelStyle: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                labelColor: color,
                unselectedLabelColor: const Color(0xff5e616b),
                indicatorSize: TabBarIndicatorSize.label,
                labelPadding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                tabs: tabs.map((e) => Text(e)).toList(),
                isScrollable: isScrollable,
              )),
        ));
  }

  static Future bottomSheet(List list) {
    return Get.bottomSheet(CupertinoActionSheet(
      actions: list
          .map((e) => CupertinoActionSheetAction(
                child: Text(e?.toString() ?? ''),
                onPressed: () {
                  Get.back(result: list.indexOf(e));
                },
              ))
          .toList(),
      cancelButton: CupertinoActionSheetAction(
        child: const Text('取消'),
        onPressed: () {
          Get.back(result: -1);
        },
      ),
    ));
  }

  static void selDate(DateValueCallback onConfirm) {
    DatePicker.showDatePicker(
      Get.context!,
      locale: DateTimePickerLocale.zh_cn,
      dateFormat: 'yyyy-MM-dd',
      onConfirm: (dateTime, selectedIndex) async {
        await Navigator.maybePop(Get.context!);
        onConfirm.call(dateTime, selectedIndex);
      },
    );
  }
}
